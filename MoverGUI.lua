local gs=LibStub("GuiSkin-1.0");
local RM=LibStub("AceAddon-3.0"):GetAddon("Raid Mover");
local RMLDB = LibStub("LibDataBroker-1.1")
local RMIcon = LibStub("LibDBIcon-1.0", true)

local changeslocked={
	state=false, 
	["playerswap"]="You must finish swapping players before making that change!", 
	["groupswap"]="You must finish swapping groups before making that change!", 
	["addingplayer"]="You must finish adding a player before making that change!", 
	["editingplayer"]="You must finish editing the player before making that change!", 
	["any"]="You are making a change and must finish or cancel that action first",
}
local classtexturepath =  {
	["Monk"]=[[Interface\Addons\RaidMover\Textures\Monk.tga]],
	["NA"]=[[Interface\BUTTONS\UI-MicroButton-Help-Up]],
	["Druid"]=[[Interface\BUTTONS\UI-MicroButton-Druid]],
	["Death Knight"]=[[Interface\BUTTONS\UI-MicroButton-DeathKnight]],
	["Warrior"]=[[Interface\BUTTONS\UI-MicroButton-Warrior]],
	["Warlock"]=[[Interface\BUTTONS\UI-MicroButton-Warlock]],
	["Mage"]=[[Interface\BUTTONS\UI-MicroButton-Mage]],
	["Priest"]=[[Interface\BUTTONS\UI-MicroButton-Priest]],
	["Paladin"]=[[Interface\BUTTONS\UI-MicroButton-Paladin]],
	["Shaman"]=[[Interface\BUTTONS\UI-MicroButton-Shaman]],
	["Rogue"]=[[Interface\BUTTONS\UI-MicroButton-Rogue]],
	["Hunter"]=[[Interface\BUTTONS\UI-MicroButton-Hunter]],
}
local tooltips={
	["saveslist"]={
		[1]=RM.color["LIGHTRED"] .. "Dropdown|r to select a saved layout",
	},
	["exportlayout"]={
		[1]=RM.color["LIGHTRED"] .. "Click|r to export selected layout",
		[2]=RM.color["LIGHTRED"] .. "CTRL + Click|r to export "..RM.color["CYAN"].."ALL|r layouts",
	},
	["importlayout"]={
		[1]=RM.color["LIGHTRED"] .. "Popout|r window to paste a previously exported layout",
	},
	["exportxml"]={
		[1]=RM.color["LIGHTRED"] .. "Export|r attendance records to "..RM.color["CYAN"].."XML|r format",
	},
	["importxml"]={
		[1]=RM.color["LIGHTRED"] .. "Overwrite|r attendance records using an Exported "..RM.color["CYAN"].."XML|r save",
	},
	["exportlua"]={
		[1]=RM.color["LIGHTRED"] .. "Export|r attendance records as a "..RM.color["CYAN"].."LUA|r table",
	},
	["importlua"]={
		[1]=RM.color["LIGHTRED"] .. "Overwrite|r attendance records using an Exported "..RM.color["CYAN"].."LUA|r save",
	},
	["promotelistprint"]={
		[1]=RM.color["LIGHTRED"] .. "Print|r raid assistant list to chat frame",
	},
	["showraid"]={
		[1]=RM.color["LIGHTRED"] .. "Display|r the current raid",
	},
	["blankraid"]={
		[1]=RM.color["LIGHTRED"] .. "Create|r a blank raid to edit",
	},
	["restoreraid"]={
		[1]=RM.color["CYAN"].."OUT OF COMBAT|r",
		[2]=RM.color["LIGHTRED"] .. "Restore|r the selected layout ",
	},
	["saveraid"]={
		[1]=RM.color["LIGHTRED"] .. "Click|r for save options",
	},
	["clearraid"]={
		[1]=RM.color["LIGHTRED"] .. "Wipe|r the current layout",
	},
	["invite"]={
		[1]=RM.color["LIGHTRED"] .. "Mass|r invite the current layout",
	},
	["share"]={
		[1]=RM.color.LIGHTRED.."Send|r the current layout to all "..RM.color.YELLOW.."RaidMover|r's in the raid."
	},
	["overwrite"]={
		[1]=RM.color["LIGHTRED"] .. "Overwrite|r the current layout",
	},
	["newsave"]={
		[1]=RM.color["LIGHTRED"] .. "Save|r as a new layout using text box above",
	},
	["difficulty"]={
		[1]=RM.color["LIGHTRED"] .. "Click|r to change Raid Difficulty",
	},
	["threshold"]={
		[1]=RM.color["LIGHTRED"] .. "Click|r to change looting threshold",
	},
	["masterlooter"]={
		[1]=RM.color["LIGHTRED"] .. "Click|r to edit Master Looter name",
	},
	["masterlooterbox"]={
		[1]=RM.color["LIGHTRED"] .. "Esc|r to cancel",
	},
	["autoattend"]={
		[1]=RM.color["LIGHTRED"] .. "DBM Enabled|r",
	},
	["autoattenddis"]={
		[1]=RM.color["LIGHTRED"] .. "DBM Not Installed|r",
	},
	["stop"]={
		[1]=RM.color["LIGHTRED"] .. "STOP|r all in progress actions",
	},
	["editboxbutton"]={
		[1]=RM.color["LIGHTRED"] .. "Import|r pasted text",
	},
}
local RMLauncher = RMLDB:NewDataObject("Raid Mover", 
	{label="Raid Mover",
	type = "data source",
	text = "Raid Mover",
	icon = "Interface\\AddOns\\RaidMover\\Textures\\piggyicon",
	OnClick = function(clickedframe, button)
		if button == "RightButton" then RM:MinimapMenu() else RM:ToggleMover() end
	end,
	OnTooltipShow = function(tt)
		tt:AddLine("Raid Mover")
		tt:AddLine(RM.color["LIGHTRED"] .. "Click|r to toggle Raid Mover window")
		tt:AddLine(RM.color["LIGHTRED"] .. "Right-click|r for quick menu")
	end,
	}
)

local templayout
local layoutselected
local emptyslots={}
local playerchanges={}
local classpicked="Monk"
local v={};

local options={
	hide=function()
		v.autoattend:Hide()
		v.autopromote:Hide()
		v.convertraidframe:Hide()
		v.automastloot:Hide()
		v.disabletooltips:Hide()
		v.disablelayouts:Hide()
		v.promoteaddButton:Hide()
		v.promoteprintButton:Hide()
		v.promoteremoveButton:Hide()
		v.attendanceTestButton:Hide()
		v.attendanceExportXMLButton:Hide()
		v.attendanceExportLUAButton:Hide()
		v.attendanceImportButton:Hide()
		v.attendanceImportButtonXML:Hide()
		v.difficultyframe:Hide()
		v.thresholdframe:Hide()
		v.mastlootframe:Hide()
	end,
	show=function()
		v.autoattend:Show()
		v.autopromote:Show()
		v.convertraidframe:Show()
		v.automastloot:Show()
		v.disabletooltips:Show()
		v.disablelayouts:Show()
		v.promoteaddButton:Show()
		v.promoteprintButton:Show()
		v.promoteremoveButton:Show()
		v.attendanceTestButton:Show()
		v.attendanceExportXMLButton:Show()
		v.attendanceExportLUAButton:Show()
		v.attendanceImportButton:Show()
		v.attendanceImportButtonXML:Show()
		v.difficultyframe:Show()
		v.thresholdframe:Show()
		v.mastlootframe:Show()
	end,
	list={v.autoattend,v.autopromote,v.convertraidframe,v.automastloot,v.disabletooltips,v.disablelayouts,v.promoteaddButton,v.promoteprintButton,v.promoteremoveButton,v.attendanceTestButton,v.attendanceExportXMLButton,v.attendanceExportLUAButton,v.attendanceImportButton,v.attendanceImportButtonXML,v.difficultyframe,v.thresholdframe,v.mastlootframe,},
}

local topbuttons={

}
local bottombuttons={

}

function RM:CreateMover()
	for g=1,8 do
		for i=1,5 do
			emptyslots[g..i]=true
		end
	end
	RM.mainFrame=CreateFrame("Frame","RaidMoverGUIframe",UIParent);
	table.insert(_G.UISpecialFrames, "RaidMoverGUIframe");
	local f = RM.mainFrame;
	v = self.view
	local swapplayer = nil
	local swapgroup = nil
	f:SetWidth(426);
	f:SetHeight(382);
	f:Hide();
	f:EnableMouse(true);
	f:SetMovable(true);
	f:SetPoint('TOPRIGHT',UIParent,'TOPRIGHT',-100 ,-300)
	f:SetScript("OnMouseDown", function(self) self:StartMoving(); end)
	f:SetScript("OnMouseUp", function(self) self:StopMovingOrSizing(); end)
	f:SetScript("OnShow", function(self) PlaySound("igCharacterInfoOpen"); end)
	f:SetScript("OnHide",function(self) PlaySound("igCharacterInfoClose"); end)
	f:SetFrameStrata("LOW");
	f:SetToplevel(true);
	
--EDITBOX IMPORT BUTTON
	v.editboxButton = CreateFrame("Button",self.ver.."editboxButton", EditBox, "UIPanelButtonTemplate");
	v.editboxButton:SetPoint("TOPLEFT",EditBox,"TOPLEFT",220,0);
	v.editboxButton:SetText("Import Text")
	v.editboxButton:SetWidth(98);
	v.editboxButton:Hide()
--//
	--[[BORDER textures part
	--There are 3 levels of layers: BACKGROUND is in the back, ARTWORK is in the middle and OVERLAY is in front. If you want to be sure that a object is before another, you must specify the level where you want to place it.
		-- BACKGROUND - Level 0. Place the background of your frame here. 
		-- BORDER - Level 1. Place the artwork of your frame here . 
		-- ARTWORK - Level 2. Place the artwork of your frame here. 
		-- OVERLAY - Level 3. Place your text, objects, and buttons in this level. 
		-- HIGHLIGHT - Level 4. Place your text, objects, and buttons in this level. 
	-- Elements in the HIGHLIGHT Layer are automatically shown or hidden when the mouse enters or leaves.
	-- For Highlighting to work you need enableMouse="true" in your <Frame> attributes.
	-- Layer can be set on creation using the <Layers> tag in XML, or the "layer" parameter in Frame:CreateTexture(). To change the layer after creation, use the LayeredRegion:SetDrawLayer() function.]]
	
	v.logoTexture=gs.CreateTexture(f,self.ver.."_logoTexture","BACKGROUND",78,78,"TOPLEFT", f, "TOPLEFT", -5,12,[[Interface\Addons\RaidMover\Textures\Piggy.tga]]);
	--620
	v.mainTexture=gs.CreateTexture(f,self.ver.."_mainTexture","BORDER",620,620,"TOPLEFT", f, "TOPLEFT", -10,14,[[Interface\TUTORIALFRAME\UI-HELP-FRAME-POPUP]]);
	
--GROUP BORDER TEXTURE AND NUMBER CREATION
	local groupborder={}
	groupborder.x = {9, 145, 281, 9, 145, 281, 9, 145}
	groupborder.y = {-70, -70, -70, -170, -170, -170, -270, -270}
	local groupnumber={}
	groupnumber.x = {136, 272, 408, 136, 272, 408, 136, 272, 408}
	groupnumber.y = {-76, -76, -76, -176, -176, -176, -276, -276, -276}

	for i=1,8 do
		v["border"..i.."Texture"] = gs.CreateTexture(f,self.ver.."_Border"..i.."Texture","ARTWORK",150,159,"TOPLEFT",f,"TOPLEFT",groupborder.x[i],groupborder.y[i],[[Interface\BattlefieldFrame\UI-BattlefieldMinimap-Border]]);
		v["group"..i.."number"]=gs.CreateFontString(f,self.ver.."_group"..i,"OVERLAY",i,"TOPLEFT",f,"TOPLEFT",groupnumber.x[i],groupnumber.y[i])
		v["group"..i.."number"]:SetFont([[Fonts\ARIALN.ttf]],19)
	end
--//

--TITLE
	v.title=gs.CreateFontString(f,self.ver.."_Title","ARTWORK","Raid Mover","TOPLEFT",f,"TOPLEFT",80,-35)
	v.title:SetFont([[Fonts\ARIALN.ttf]],18)
--//
	
--CLOSE 'X' button
	v.closeButton = CreateFrame("Button",self.ver.."closeButton", f, "UIPanelCloseButton");
	v.closeButton:SetPoint("TOPRIGHT",f,"TOPRIGHT",7,2);
	v.closeButton:SetWidth(35);
	v.closeButton:SetHeight(35);
	--//
	
--CONFIG button
	v.configButton = CreateFrame("Button",self.ver.."configButton", f, "UIPanelButtonTemplate");
	v.configButton:SetPoint("TOPRIGHT",f,"TOPRIGHT",-24,-3);
	v.configButton:SetText("Profiles")
	v.configButton:SetWidth(58);
	v.configButton:SetScript("OnClick",function(self)
		RM:showConfigWindow()
	  end)
	--//
	
--STOP button
	v.stopButton = CreateFrame("Button",self.ver.."stopButton", f, "UIPanelButtonTemplate");
	v.stopButton:SetPoint("TOPRIGHT",f,"TOPRIGHT",-79,-3);
	v.stopButton:SetText("STOP")
	v.stopButton:SetWidth(58);
	v.stopButton:SetScript("OnClick",function(self)
		RM:CancelAllTimers()
		RM:Print("Stopping all current actions!")
	  end)
	v.stopButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"stop") end)
	v.stopButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
--//
	
--EXPORT LAYOUT Button
	v.exportlayoutButton = CreateFrame("Button", self.ver.."ButtonExport", f, "UIPanelButtonTemplate")
	v.exportlayoutButton:SetText("Export")
	v.exportlayoutButton:SetPoint('TOPRIGHT', f,'TOPRIGHT', -185,-3)
	v.exportlayoutButton:SetWidth(58)
	v.exportlayoutButton:SetScript("OnClick", function(self, button)
		if IsControlKeyDown() then
			local allstring=""
			for layouts, layoutname in pairs(pDB.layoutlist) do
				allstring=allstring..RM:exportLayout(layoutname).."\n"
			end
			RM:PopupWindow(allstring)
		else
			if layoutselected then
				RM:PopupWindow(RM:exportLayout(layoutselected))
			end
		end
	  end)
	v.exportlayoutButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"exportlayout") end)
	v.exportlayoutButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
--//

--IMPORT LAYOUT Button
	v.importlayoutButton = CreateFrame("Button", self.ver.."ButtonExport", f, "UIPanelButtonTemplate")
	v.importlayoutButton:SetText("Import")
	v.importlayoutButton:SetPoint('TOPRIGHT', f,'TOPRIGHT', -240,-3)
	v.importlayoutButton:SetWidth(58)
	v.importlayoutButton:SetScript("OnClick",function(self)
		RM:PopupWindow()
	  end)
	v.importlayoutButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"importlayout") end)
	v.importlayoutButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
--//

--SAVES DROP DOWN Button
	v.savesButton = CreateFrame("Button", self.ver.."ButtonSaves", f, "UIPanelButtonTemplate")
	v.savesButton:SetText("Saves")
	v.savesButton:SetPoint('TOPRIGHT', f,'TOPRIGHT', -295,-3)
	v.savesButton:SetWidth(58)
	v.savesButton:SetScript("OnClick",function(self)
		if changeslocked.state==false then
			if playerchanges.are==true then
				changeslocked.state="any"
				v.raidButton:Hide()
				v.restoreButton:Hide()
				v.saveButton:Hide()
				v.clearButton:Hide()
				
				if not v.clearnoButton then
					v.clearnoButton = CreateFrame("Button", RM.ver.."clearnoButton", f, "UIPanelButtonTemplate")
					v.clearyesButton = CreateFrame("Button", RM.ver.."clearyesButton", f, "UIPanelButtonTemplate")
					v.clearText=gs.CreateFontString(f,RM.ver.."clearText","ARTWORK","Unsaved Changes\nAre you sure?","TOPLEFT",f,"TOPLEFT",304,-280)
					v.clearText:SetFont([[Fonts\ARIALN.ttf]],14)
				
					v.clearnoButton:SetText("CANCEL")
					v.clearnoButton:SetPoint('TOPLEFT', f,'TOPLEFT', 317,-316)
					v.clearnoButton:SetWidth(105)
					v.clearnoButton:SetHeight(40)
					
					v.clearyesButton:SetText("Yes")
					v.clearyesButton:SetPoint('TOPLEFT', f,'TOPLEFT', 284,-324)
					v.clearyesButton:SetWidth(33)
				else
					v.clearnoButton:Show()
					v.clearyesButton:Show()
					v.clearText:Show()
				end
				
				v.clearnoButton:SetScript("OnClick",function(self)
					changeslocked.state=false
					v.clearnoButton:Hide()
					v.clearyesButton:Hide()
					v.clearText:Hide()
					v.raidButton:Show()
					v.restoreButton:Show()
					v.saveButton:Show()
					v.clearButton:Show()
				end)
				
				v.clearyesButton:SetScript("OnClick",function(self)
					changeslocked.state=false
					v.clearnoButton:Hide()
					v.clearyesButton:Hide()
					v.clearText:Hide()
					v.raidButton:Show()
					v.restoreButton:Show()
					v.saveButton:Show()
					v.clearButton:Show()
					RM:SavesMenu()
				end)
			else
				RM:SavesMenu()
			end
		else
			RM:Print(changeslocked[changeslocked.state])
		end
	  end)
	v.savesButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"saveslist") end)
	v.savesButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
--//

--DELETE BUTTON
	v.deleteButton = CreateFrame("Button",self.ver.."deleteButton", f, "UIPanelButtonTemplate");
	v.deleteButton:SetPoint("TOPRIGHT",f,"TOPRIGHT",-57,-29);
	v.deleteButton:SetText("Delete")
	v.deleteButton:SetWidth(55);
	v.deleteButton:SetScript("OnClick",function(self)
		if changeslocked.state==false then
			changeslocked.state="any"
			v.renameButton:Hide()
			v.inviteButton:Hide()
			v.deleteButton:Hide()
			v.shareButton:Hide()
			
			if not v.delyesButton then
				v.deleteText=gs.CreateFontString(f,RM.ver.."deleteText","ARTWORK","Are you sure?","TOPRIGHT",f,"TOPRIGHT",-135,-30)
				v.deleteText:SetFont([[Fonts\ARIALN.ttf]],14)
				
				v.delyesButton = CreateFrame("Button",RM.ver.."delyesButton", f, "UIPanelButtonTemplate");
				v.delyesButton:SetPoint("TOPRIGHT",f,"TOPRIGHT",-128,-47);
				v.delyesButton:SetText("Yes")
				v.delyesButton:SetWidth(30);
				
				v.delnoButton = CreateFrame("Button",RM.ver.."delnoButton", f, "UIPanelButtonTemplate");
				v.delnoButton:SetPoint("TOPRIGHT",f,"TOPRIGHT",-5,-30);
				v.delnoButton:SetText("CANCEL")
				v.delnoButton:SetWidth(120);
				v.delnoButton:SetHeight(40);
			else
				v.delnoButton:Show()
				v.delyesButton:Show()
				v.deleteText:Show()
			end
			
			v.delyesButton:SetScript("OnClick",function(self)
				changeslocked.state=false
				PlaySoundFile([[Sound\Interface\Ui_Etherealwindow_Open.ogg]],"SFX")
				RM:removeLayout(layoutselected)
				playerchanges = {}
				RM:WipeList()
				v.title:SetText("Raid Mover")
			end)

			v.delnoButton:SetScript("OnClick",function(self)
				changeslocked.state=false
				v.delnoButton:Hide()
				v.delyesButton:Hide()
				v.deleteText:Hide()
				v.renameButton:Show()
				v.inviteButton:Show()
				v.deleteButton:Show()
				v.shareButton:Show()
			end)
		else
			RM:Print(changeslocked[changeslocked.state])
		end
	end)
--//

--RENAME BUTTON
	v.renameButton = CreateFrame("Button",self.ver.."renameButton", f, "UIPanelButtonTemplate");
	v.renameButton:SetPoint("TOPRIGHT",f,"TOPRIGHT",-111,-29);
	v.renameButton:SetText("Rename")
	v.renameButton:SetWidth(55);
	v.renameButton:SetScript("OnClick",function(self)
		if changeslocked.state==false then
			changeslocked.state="any"
			--CREATE NEW TEXT FIELD HERE FOR THE RENAME PROCESS
			v.renameButton:Hide()
			v.inviteButton:Hide()
			v.shareButton:Hide()
			v.deleteButton:Hide()
			if not v.renametextBox then
				v.renametextBox = CreateFrame("EditBox", RM.ver.."renametextBox", f, "InputBoxTemplate")
				v.renametextBox:SetPoint('TOPRIGHT', f,'TOPRIGHT', -6,-27)
				v.renametextBox:SetCursorPosition(0)
				v.renametextBox:SetWidth(145)
				v.renametextBox:SetHeight(21)
				v.renametextBox:SetAutoFocus(false)
				
				v.renameyesButton = CreateFrame("Button",RM.ver.."renameyesButton", f, "UIPanelButtonTemplate");
				v.renameyesButton:SetPoint("TOPRIGHT",f,"TOPRIGHT",-128,-47);
				v.renameyesButton:SetText("Ok")
				v.renameyesButton:SetWidth(30);
				
				v.renamenoButton = CreateFrame("Button",RM.ver.."renamenoButton", f, "UIPanelButtonTemplate");
				v.renamenoButton:SetPoint("TOPRIGHT",f,"TOPRIGHT",-3,-47);
				v.renamenoButton:SetText("CANCEL")
				v.renamenoButton:SetWidth(120);
			else
				v.renamenoButton:Show()
				v.renameyesButton:Show()
				v.renametextBox:Show()
			end
			v.renametextBox:SetText(layoutselected)
			
			v.renametextBox:SetScript("OnEscapePressed",function(self)
				v.renametextBox:SetText("")
				self:ClearFocus();
			end)
			
			v.renametextBox:SetScript("OnEnterPressed",function(self)
				if v.renametextBox:GetText() == nil or v.renametextBox:GetText() == "" then
					RM:Print("You must type a new layout name!")
				else
					PlaySoundFile([[Interface\Addons\RaidMover\Sounds\CubeChing.ogg]],"SFX")
					changeslocked.state=false
					RM:renameLayout(layoutselected, v.renametextBox:GetText())
					RM:SavesDisplay(v.renametextBox:GetText())
				end
			end)
			
			v.renameyesButton:SetScript("OnClick",function(self)
				if v.renametextBox:GetText() == nil or v.renametextBox:GetText() == "" then
					RM:Print("You must type a new layout name!")
				else
					PlaySoundFile([[Interface\Addons\RaidMover\Sounds\CubeChing.ogg]],"SFX")
					changeslocked.state=false
					RM:renameLayout(layoutselected, v.renametextBox:GetText())
					RM:SavesDisplay(v.renametextBox:GetText())
				end
			end)

			
			v.renamenoButton:SetScript("OnClick",function(self)
				changeslocked.state=false
				v.renamenoButton:Hide()
				v.renameyesButton:Hide()
				v.renametextBox:Hide()
				v.renameButton:Show()
				v.inviteButton:Show()
				v.shareButton:Show()
				v.deleteButton:Show()
			end)
			
			
		else
			RM:Print(changeslocked[changeslocked.state])
		end
	end)
--//

--INVITE BUTTON
	v.inviteButton = CreateFrame("Button",self.ver.."inviteButton", f, "UIPanelButtonTemplate");
	v.inviteButton:SetPoint("TOPRIGHT",f,"TOPRIGHT",-3,-29);
	v.inviteButton:SetText("Invite")
	v.inviteButton:SetWidth(55);
	v.inviteButton:SetScript("OnClick",function(self)
		--if layoutselected then
		--	RM:inviteRaid(layoutselected)
		if templayout then
			RM:inviteRaid(RM:EmptySlots("remove", RM:deepCopy(templayout, {})))
		end
	end)
	v.inviteButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"invite") end)
	v.inviteButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
--//

--SHARE BUTTON
	v.shareButton = CreateFrame("Button",self.ver.."shareButton", f, "UIPanelButtonTemplate");
	v.shareButton:SetPoint("TOPRIGHT",f,"TOPRIGHT",-3,-50);
	v.shareButton:SetText("Share")
	v.shareButton:SetWidth(55);
	v.shareButton:SetScript("OnClick",function(self)
		PlaySoundFile([[Interface\Addons\RaidMover\Sounds\HolyBolt1.ogg]],"SFX");
		if layoutselected then
			--Send(msg, layoutname, layouttable, channel, target)
			RM:Send("sendlayout",layoutselected, RM:EmptySlots("remove", RM:deepCopy(templayout, {})), "RAID")
		elseif templayout then
			RM:Send("sendlayout","Raid", RM:EmptySlots("remove", RM:deepCopy(templayout, {})), "RAID")
		end
	end)
	v.shareButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"share") end)
	v.shareButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
	v.shareButton:Disable()
--//

--SHOW RAID BUTTON
	v.raidButton = CreateFrame("Button", self.ver.."ButtonCurrentRaid", f, "UIPanelButtonTemplate")
	v.raidButton:SetText("Show Raid")
	v.raidButton:SetPoint('TOPLEFT', f,'TOPLEFT', 284,-273)
	v.raidButton:SetWidth(81)
	v.raidButton:SetScript("OnClick",function(self)
		if changeslocked.state==false then
			if IsInRaid() then
				if playerchanges.are==true then
					changeslocked.state="any"
					v.raidButton:Hide()
					v.restoreButton:Hide()
					v.saveButton:Hide()
					v.clearButton:Hide()
					
					if not v.clearnoButton then
						v.clearnoButton = CreateFrame("Button", RM.ver.."clearnoButton", f, "UIPanelButtonTemplate")
						v.clearyesButton = CreateFrame("Button", RM.ver.."clearyesButton", f, "UIPanelButtonTemplate")
						v.clearText=gs.CreateFontString(f,RM.ver.."clearText","ARTWORK","Unsaved Changes\nAre you sure?","TOPLEFT",f,"TOPLEFT",304,-280)
						v.clearText:SetFont([[Fonts\ARIALN.ttf]],14)
					
						v.clearnoButton:SetText("CANCEL")
						v.clearnoButton:SetPoint('TOPLEFT', f,'TOPLEFT', 317,-316)
						v.clearnoButton:SetWidth(105)
						v.clearnoButton:SetHeight(40)
						
						v.clearyesButton:SetText("Yes")
						v.clearyesButton:SetPoint('TOPLEFT', f,'TOPLEFT', 284,-324)
						v.clearyesButton:SetWidth(33)
					else
						v.clearnoButton:Show()
						v.clearyesButton:Show()
						v.clearText:Show()
					end
					
					v.clearnoButton:SetScript("OnClick",function(self)
						changeslocked.state=false
						v.clearnoButton:Hide()
						v.clearyesButton:Hide()
						v.clearText:Hide()
						v.raidButton:Show()
						v.restoreButton:Show()
						v.saveButton:Show()
						v.clearButton:Show()
					end)
					
					v.clearyesButton:SetScript("OnClick",function(self)
						changeslocked.state=false
						v.clearnoButton:Hide()
						v.clearyesButton:Hide()
						playerchanges = {}
						raidlayout, _ = RM:getCurrentRaid()
						if type(raidlayout) == "table" then
							PlaySoundFile([[Interface\Addons\RaidMover\Sounds\AddSocketQuest.ogg]],"SFX")
							RM:SavesDisplay(raidlayout)
						end
					end)
				else
					playerchanges = {}
					raidlayout, _ = RM:getCurrentRaid()
					if type(raidlayout) == "table" then
						PlaySoundFile([[Interface\Addons\RaidMover\Sounds\AddSocketQuest.ogg]],"SFX")
						RM:SavesDisplay(raidlayout)
					end
				end
			else
				RM:Print("You are not in a raid")
			end
		else
			RM:Print(changeslocked[changeslocked.state])
		end
	  end)
	v.raidButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"showraid") end)
	v.raidButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
	  --//
	
--BLANK TEMPLATE BUTTON	
	v.blankButton = CreateFrame("Button", self.ver.."BlankButton", f, "UIPanelButtonTemplate")
	v.blankButton:SetText("Blank Raid")
	v.blankButton:SetPoint('TOPLEFT', f,'TOPLEFT', 284,-297)
	v.blankButton:SetWidth(81)
	v.blankButton:SetScript("OnClick",function(self)
		PlaySoundFile([[Interface\Addons\RaidMover\Sounds\AddSocketQuest.ogg]],"SFX"); 
		RM:SavesDisplay(RM:EmptySlots("blank", {}))
	  end)
	v.blankButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"blankraid") end)
	v.blankButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
--//

--RESTORE BUTTON
	v.restoreButton = CreateFrame("Button", self.ver.."ButtonRestore", f, "UIPanelButtonTemplate")
	v.restoreButton:SetText("Restore")
	v.restoreButton:SetPoint('TOPLEFT', f,'TOPLEFT', 284,-297)
	v.restoreButton:SetWidth(81)
	v.restoreButton:SetScript("OnClick",function(self)
		RM:startMover(RM:EmptySlots("remove", RM:deepCopy(templayout, {})))
	  end)
	v.restoreButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"restoreraid") end)
	v.restoreButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
--//
	
--SAVE BUTTON
	v.saveButton = CreateFrame("Button", self.ver.."ButtonSave", f, "UIPanelButtonTemplate")
	v.saveButton:SetText("Save")
	v.saveButton:SetPoint('TOPLEFT', f,'TOPLEFT', 284,-321)
	v.saveButton:SetWidth(81)
	v.saveButton:SetScript("OnClick",function(self)
		if changeslocked.state==false then
			changeslocked.state="any"
			--Hide Show Raid, Restore, Save, Clear buttons
			v.raidButton:Hide()
			v.restoreButton:Hide();
			v.saveButton:Hide();
			v.clearButton:Hide();
			--Show 3 buttons, New, Overwrite, Cancel.
			v.savetextBox:SetText("")
			v.savetextBox:Show()
			v.newsaveButton:Show()
			v.overwriteButton:Show()
			v.cancelButton:Show()
		else
			RM:Print(changeslocked[changeslocked.state])
		end
	end)
	v.saveButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"saveraid") end)
	v.saveButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
--//
	
--CLEAR Button
	v.clearButton = CreateFrame("Button", self.ver.."clearButton", f, "UIPanelButtonTemplate")
	v.clearButton:SetText("Clear")
	v.clearButton:SetPoint('TOPLEFT', f,'TOPLEFT', 284,-345)
	v.clearButton:SetWidth(81)
	v.clearButton:SetScript("OnClick",function(self)
		if changeslocked.state==false then
			if playerchanges.are==true then
				changeslocked.state="any"
				v.raidButton:Hide()
				v.restoreButton:Hide()
				v.saveButton:Hide()
				v.clearButton:Hide()
				
				if not v.clearnoButton then
					v.clearnoButton = CreateFrame("Button", RM.ver.."clearnoButton", f, "UIPanelButtonTemplate")
					v.clearyesButton = CreateFrame("Button", RM.ver.."clearyesButton", f, "UIPanelButtonTemplate")
					v.clearText=gs.CreateFontString(f,RM.ver.."clearText","ARTWORK","Unsaved Changes\nAre you sure?","TOPLEFT",f,"TOPLEFT",304,-280)
					v.clearText:SetFont([[Fonts\ARIALN.ttf]],14)
				
					v.clearnoButton:SetText("CANCEL")
					v.clearnoButton:SetPoint('TOPLEFT', f,'TOPLEFT', 317,-316)
					v.clearnoButton:SetWidth(105)
					v.clearnoButton:SetHeight(40)
					
					v.clearyesButton:SetText("Yes")
					v.clearyesButton:SetPoint('TOPLEFT', f,'TOPLEFT', 284,-324)
					v.clearyesButton:SetWidth(33)
				else
					v.clearnoButton:Show()
					v.clearyesButton:Show()
					v.clearText:Show()
				end
				
				v.clearnoButton:SetScript("OnClick",function(self)
					changeslocked.state=false
					v.clearnoButton:Hide()
					v.clearyesButton:Hide()
					v.clearText:Hide()
					v.raidButton:Show()
					v.restoreButton:Show()
					v.saveButton:Show()
					v.clearButton:Show()
				end)
				
				v.clearyesButton:SetScript("OnClick",function(self)
					changeslocked.state=false
					v.clearnoButton:Hide()
					v.clearyesButton:Hide()
					PlaySoundFile([[Interface\Addons\RaidMover\Sounds\Transmute.ogg]],"SFX")
					playerchanges = {}
					RM:WipeList()
					v.title:SetText("Raid Mover")
				end)
			else
				PlaySoundFile([[Interface\Addons\RaidMover\Sounds\Transmute.ogg]],"SFX")
				playerchanges = {}
				RM:WipeList()
				v.title:SetText("Raid Mover")
			end
		else
			RM:Print(changeslocked[changeslocked.state])
		end
	  end)
	v.clearButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"clearraid") end)
	v.clearButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
	  --//
	
--SAVE NAME EDIT BOX
	v.savetextBox = CreateFrame("EditBox", self.ver.."SaveTextBox", f, "InputBoxTemplate")
	v.savetextBox:SetPoint('TOPLEFT', f,'TOPLEFT', 291,-273)
	v.savetextBox:SetAutoFocus(false);
	v.savetextBox:SetCursorPosition(0)
	v.savetextBox:SetWidth(129)
	v.savetextBox:SetHeight(21)
	v.savetextBox:SetScript("OnEscapePressed",function(self)
		self:ClearFocus();
	end)
--//
	
--NEW SAVE Button
	v.newsaveButton = CreateFrame("Button", self.ver.."ButtonNewSave", f, "UIPanelButtonTemplate")
	v.newsaveButton:SetText("^New^")
	v.newsaveButton:SetPoint('TOPLEFT', f,'TOPLEFT', 284,-297)
	v.newsaveButton:SetWidth(81)
	v.newsaveButton:SetScript("OnClick",function(self)
		if v.savetextBox:GetText() == nil or v.savetextBox:GetText() == "" then
			RM:Print("You must type a new save name")
		else
			changeslocked.state=false
			PlaySoundFile([[Interface\Addons\RaidMover\Sounds\CubeChing.ogg]],"SFX")
			RM:saveLayout(v.savetextBox:GetText(), RM:EmptySlots("remove", RM:deepCopy(templayout, {})))
			v.savetextBox:Hide()
			v.newsaveButton:Hide()
			v.overwriteButton:Hide()
			v.cancelButton:Hide()
			v.raidButton:Show()
			v.restoreButton:Show()
			v.saveButton:Show()
			v.clearButton:Show()
			RM:runSaves();
			playerchanges = {}
			RM:SavesDisplay(v.savetextBox:GetText())
		end
	  end)
	v.newsaveButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"newsave") end)
	v.newsaveButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
--//
	
--OVERWRITE Button
	v.overwriteButton = CreateFrame("Button", self.ver.."ButtonOverwrite", f, "UIPanelButtonTemplate")
	v.overwriteButton:SetText("Overwrite")
	v.overwriteButton:SetPoint('TOPLEFT', f,'TOPLEFT', 284,-321)
	v.overwriteButton:SetWidth(81)
	v.overwriteButton:SetScript("OnClick",function(self)
		if layoutselected then
			changeslocked.state=false
			PlaySoundFile([[Interface\Addons\RaidMover\Sounds\Repair.ogg]],"SFX")
			RM:saveLayout(layoutselected, RM:EmptySlots("remove", RM:deepCopy(templayout, {})))
			v.savetextBox:Hide()
			v.newsaveButton:Hide()
			v.overwriteButton:Hide()
			v.cancelButton:Hide()
			v.raidButton:Show()
			v.restoreButton:Show()
			v.saveButton:Show()
			v.clearButton:Show()
			playerchanges = {}
			RM:SavesDisplay(layoutselected)
		else
			RM:Print("There's nothing to overwrite!")
		end
	  end)
	v.overwriteButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"overwrite") end)
	v.overwriteButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
--//
	
--CANCEL Button
	v.cancelButton = CreateFrame("Button", self.ver.."ButtonCancel", f, "UIPanelButtonTemplate")
	v.cancelButton:SetText("Cancel")
	v.cancelButton:SetPoint('TOPLEFT', f,'TOPLEFT', 284,-345)
	v.cancelButton:SetWidth(81)
	v.cancelButton:SetScript("OnClick",function(self)
		changeslocked.state=false
		v.savetextBox:Hide()
		v.newsaveButton:Hide()
		v.overwriteButton:Hide()
		v.cancelButton:Hide()
		v.raidButton:Show()
		v.restoreButton:Show()
		v.saveButton:Show()
		v.clearButton:Show()
	  end)
	--//
	v.savetextBox:Hide()
	v.newsaveButton:Hide()
	v.overwriteButton:Hide()
	v.cancelButton:Hide()
	v.restoreButton:Hide()
	v.saveButton:Hide()
	v.clearButton:Hide()
	v.deleteButton:Hide()
	v.renameButton:Hide()
	v.inviteButton:Hide()
	v.shareButton:Hide()
	
	--[[
	TOP", "RIGHT" "BOTTOM" "LEFT": the center-points of the respective sides.
	"TOPRIGHT", "TOPLEFT", "BOTTOMLEFT", "BOTTOMRIGHT": corners of the frame rectangle.
	"CENTER": the center point of the frame rectangle.
	]]

--BOTTOM TAB FRAMES
	local activetab_y=-388
	local activetab_x={["main"]=26, ["options"]=95}
	v.activetab=CreateFrame("Frame",self.ver.."_ActiveTab",f);
	v.activetab:SetPoint("TOPLEFT",f,"TOPLEFT",activetab_x["main"],activetab_y);
	v.activetab:SetWidth(45);
	v.activetab:SetHeight(20);
	v.activetabtexture = gs.CreateTexture(f,self.ver.."_MainTabTexture","BACKGROUND",74,57,"CENTER",v.activetab,"CENTER",0,-14,[[Interface/PaperDollInfoFrame/UI-Character-ActiveTab]]);
	
	v.maintab=CreateFrame("Frame",self.ver.."_MainTab",f);
	v.maintab:SetPoint("TOPLEFT",f,"TOPLEFT",25,-388);
	v.maintab:SetWidth(45);
	v.maintab:SetHeight(20);
	v.maintabtexture = gs.CreateTexture(f,self.ver.."_MainTabTexture","BACKGROUND",74,28,"CENTER",v.maintab,"CENTER",0,0,[[Interface/PaperDollInfoFrame/UI-Character-InActiveTab]]);
	v.maintabtext = gs.CreateFontString(f,self.ver.."_MainTabText","OVERLAY","Mover","CENTER",v.maintab,"CENTER",0,-1)
	v.maintabtext:SetFont([[Fonts\ARIALN.ttf]],15)
	v.maintabtexture:Hide()
	v.maintab:SetScript("OnMouseDown", function(self, button)
		if v.maintabtexture:IsShown() then
			PlaySoundFile([[Sound\Interface\Ucharactersheettab.ogg]],"SFX")
			RM:ShowMover()
			v.maintabtexture:Hide()
			v.optionstitle:Hide()
			v.optionstexture:Show()
			if v.autoattend then
				options.hide()
			end
			if not templayout then
				RM:WipeList()
			end
			v.activetab:SetPoint("TOPLEFT",f,"TOPLEFT",activetab_x["main"],activetab_y);
			v.maintabtext:SetPoint("CENTER",v.maintab,"CENTER",0,-1)
			v.optionstext:SetPoint("CENTER",v.optionstab,"CENTER",0,2)
		end
	end)
--//	

--OPTIONS PANEL FRAMES
	v.optionstab=CreateFrame("Frame",self.ver.."_OptionsTab",f);
	v.optionstab:SetPoint("TOPLEFT",f,"TOPLEFT",95,-388);
	v.optionstab:SetWidth(45);
	v.optionstab:SetHeight(20);
	v.optionstitle=gs.CreateFontString(f,self.ver.."_Title","ARTWORK","Options","TOPLEFT",f,"TOPLEFT",80,-35)
	v.optionstitle:SetFont([[Fonts\ARIALN.ttf]],18)
	v.optionstitle:Hide()
	v.optionstexture = gs.CreateTexture(f,self.ver.."_OptionsTexture","BACKGROUND",74,28,"CENTER",v.optionstab,"CENTER",0,0,[[Interface/PaperDollInfoFrame/UI-Character-InActiveTab]]);
	v.optionstext=gs.CreateFontString(f,self.ver.."_OptionsText","OVERLAY","Options","CENTER",v.optionstab,"CENTER",0,2)
	v.optionstext:SetFont([[Fonts\ARIALN.ttf]],15)
	v.optionstab:SetScript("OnMouseDown", function(self, button)
		if changeslocked.state==false then
			if v.optionstexture:IsShown() then
				PlaySoundFile([[Sound\Interface\Ucharactersheettab.ogg]],"SFX")
				RM:HideMover()
				v.activetab:SetPoint("TOPLEFT",f,"TOPLEFT",activetab_x["options"],activetab_y);
				v.optionstext:SetPoint("CENTER",v.optionstab,"CENTER",0,-1)
				v.maintabtext:SetPoint("CENTER",v.maintab,"CENTER",0,2)
				v.optionstexture:Hide()
				v.optionstitle:Show()
				v.maintabtexture:Show()
				RM:CreateOptions()
			end
		else
			RM:Print(changeslocked[changeslocked.state])
		end
	end)
--//
	function self:HideMover()
		for i=1,8 do
			v["border"..i.."Texture"]:Hide()
			v["group"..i.."number"]:Hide()
			if templayout then
				v["grp"..i.."frame"]:Hide();
			end
		end
		
		if templayout then
			for pname, ptable in pairs(templayout) do
				if pname and v[pname.."frame"] then
					v[pname.."frame"]:Hide();
				end
			end
			if layoutselected then
				v.renameButton:Hide()
				v.deleteButton:Hide()
			end
			v.inviteButton:Hide()
			v.shareButton:Hide()
			v.restoreButton:Hide()
			v.clearButton:Hide()
			v.saveButton:Hide()
		end
		v.title:Hide()
		v.savesButton:Disable()
		v.blankButton:Hide()
		v.raidButton:Hide()
	end

	function self:ShowMover()
		for i=1,8 do
			v["border"..i.."Texture"]:Show()
			v["group"..i.."number"]:Show()
			if templayout then
				v["grp"..i.."frame"]:Show();
			end
		end
		if templayout then
			for pname, ptable in pairs(templayout) do
				if pname and v[pname.."frame"] then
					v[pname.."frame"]:Show();
				end
			end
			if layoutselected then
				v.renameButton:Show()
				v.deleteButton:Show()
			end
			v.inviteButton:Show()
			v.shareButton:Show()
			v.restoreButton:Show()
			v.clearButton:Show()
			v.saveButton:Show()
		else
			v.blankButton:Show()
		end
		v.title:Show()
		v.savesButton:Enable()
		v.raidButton:Show()
	end
	
--OPTIONS PAGE ITEMS
	--Enable Auto Attendance Tracking
		--guild rank
		--class
		--dates
		--Way to fix attendance records
			--Dropdown to list an entire players attendance records
			
		
	--Auto Raid Assistant Promotions (NEW TAB?!)
		--Field to add/remove players
		--Dropdown with to list all players, including their class color, on the auto promotes list
		--Options to Auto Loot Master with threshold
		--Option for Auto Raid
		
	--Place to change defaults
		--Raid moving default delay
	
	
--MAIN DISPLAY WINDOW
	function self:SavesDisplay(layout)
		--Check for previous layout and clear and hide all frames(Wipe)
		if templayout or pDB.layouts[layoutselected] then
			RM:WipeList()
		end
		
		--Shows frames that may have otherwise been hidden
		if (v.restoreButton:IsShown()==nil) then
			v.restoreButton:Show()
			v.saveButton:Show()
			v.clearButton:Show()
		end
		v.blankButton:Hide()
		
		--Would be table if player used "Show Raid" or "Blank Raid"
		if type(layout) == "table" then
			templayout = RM:deepCopy(layout, {})
			v.title:SetText("Raid Mover")
			v.inviteButton:Show()
			v.shareButton:Show()
		--Else a saved layout was selected, populate Mover with that.
		else
			templayout = RM:deepCopy(pDB.layouts[layout], {})
			v.deleteButton:Show()
			v.renameButton:Show()
			v.inviteButton:Show()
			v.shareButton:Show()
			v.title:SetText(layout)
			layoutselected = layout;
		end

		--Unsaved changes that have been made to layout
		--This allows for future "Undo" options. But how will I know which was done first
		--Will have use indices([1], [2], [#]) instead of pname.
		if playerchanges.are==true then
			v.title:SetText(v.title:GetText().."*")
			for pname, ptable in pairs(playerchanges) do
				if type(ptable) == "table" then
					templayout[pname] = ptable
				else
					templayout[pname] = nil
				end
			end
			
			--Iterate through the indices
			--for i=1, #playerchanges do
				--Iterate through the table at index i [i]={pname={subrp=1, class=Mage}}
			--	for pname, ptable in pairs(playerchanges[i]) do
			--		if type(ptable) == "table" then
			--			templayout[pname] = ptable
			--		else
			--			templayout[pname] = nil
			--		end
			--	end
			--end
			--With the above table setup I can pop off the last thing I stacked for "Undo" functionality
			--And then throw that in to another table for "Redo" functionality.
			--Nifty :)
		end
		
		local subgrp, class
		local points = {}
		points.x = {17, 153, 289, 17, 153, 289, 17, 153, 289}
		points.y = {-78, -78, -78, -178, -178, -178, -278, -278, -278}
		local yoffset = -18
		
		RM:EmptySlots("add")

--PLAYER BUTTON CREATION		
		for pname, ptable in pairs(templayout) do
			subgrp, class = templayout[pname].subgrp, templayout[pname].class
--CREATE PLAYER FRAMES
			if not v[pname.."frame"] then
				v[pname.."frame"]=CreateFrame("Frame",RM.ver.."_"..pname.."frame",f);
			end
			v[pname.."frame"]:SetWidth(109);
			v[pname.."frame"]:SetHeight(19);
			v[pname.."frame"]:Show();
			v[pname.."frame"]:EnableMouse(true);
			v[pname.."frame"]:SetMovable(false);
			v[pname.."frame"]:SetResizable(false);
			v[pname.."frame"]:SetPoint('TOPLEFT',f,'TOPLEFT',points.x[subgrp], points.y[subgrp])
			points.y[subgrp]=points.y[subgrp]+yoffset
			v[pname.."frame"].selected=false;
			v[pname.."frame"]:SetScript("OnMouseDown", function(self, button)
	--RIGHT CLICK PLAYER FUNCTIONALITY
				if button == "RightButton" then
		--EMPTY SLOT "ADD PLAYER" MENU
					if not v[pname.."string"] then
						if changeslocked.state==false then
							local addfromraid={}
							if IsInRaid() then
								local iter=1
								local raidlayout, _ = RM:getCurrentRaid()
								for pname_curraid, ptable in pairs(raidlayout) do
									if not templayout[pname_curraid] then
										addfromraid[iter]={text= pname_curraid, colorCode=RM.classcolor[raidlayout[pname_curraid].class].code, notCheckable=true, func=function()
											PlaySoundFile([[Interface\Addons\RaidMover\Sounds\RingDing.ogg]],"SFX");
											changeslocked.state=false
											v[pname.."frame"]:ClearAllPoints()
											playerchanges.are=true
											playerchanges[pname_curraid] = {}
											playerchanges[pname_curraid].subgrp = templayout[pname].subgrp
											playerchanges[pname_curraid].class = raidlayout[pname_curraid].class
											if pDB.layouts[layoutselected] then
												RM:SavesDisplay(layoutselected)
											else
												RM:SavesDisplay(RM:deepCopy(templayout, {}))
											end
										end}
										iter=iter+1
									end
								end
								RM:SortFunc(addfromraid)
							else
								addfromraid[1]={text="Not in raid", notCheckable=true,}
							end
							local addplayermenu = {
						--ADD PLAYER: EDIT BOX		
								{ text="Add Player", notCheckable=true, func = function()
									PlaySoundFile([[Sound\Interface\Uescapescreenopen.ogg]],"SFX");
									changeslocked.state="addingplayer"
									local _,_,_,p1xoffset,p1yoffset = v[pname.."frame"]:GetPoint(1)
									p1xoffset=math.floor(p1xoffset)
									p1yoffset=math.floor(p1yoffset)
									if not v.addplayerBox then
										v.addplayerBox = CreateFrame("EditBox", RM.ver.."_"..pname.."AddPlayerBox", f, "InputBoxTemplate")
									end
									v.addplayerBox:SetWidth(110)
									v.addplayerBox:SetHeight(18)
									v.addplayerBox:SetFrameLevel(100)
									v.addplayerBox:SetCursorPosition(0)
									v.addplayerBox:SetPoint('TOPLEFT', f,'TOPLEFT', p1xoffset+3, p1yoffset)
									v.addplayerBox:Show()
									v.addplayerBox:SetAutoFocus(false);
									v[pname.."frame"]:Hide();
									for i=1, 8 do
										v["grp"..i.."frame"]:Hide()
									end	
						--//
						--ADD PLAYER: CLASS BUTTON DROPDOWN MENU
									if not v.classButton then
										v.classButton = CreateFrame("Button", RM.ver.."_ClassButton", f, "UIPanelButtonTemplate")
									end
									if classpicked=="Death Knight" then
										v.classButton:SetText(RM.classcolor[classpicked].code.."DKnight");
									else
										v.classButton:SetText(RM.classcolor[classpicked].code..classpicked)
									end
									v.classButton:SetWidth(61)
									v.classButton:SetFrameLevel(100)
									v.classButton:SetPoint('TOPLEFT', f,'TOPLEFT', p1xoffset-4, p1yoffset-18)
									v.classButton:Show()
									v.classButton:SetScript("OnClick",function(self)
										RM:ClassMenu()
									end)
						--//
						--ADD PLAYER: CANCEL BUTTON
									if not v.cancelclassButton then
										v.cancelclassButton = CreateFrame("Button", RM.ver.."_CancelClassButton", f, "UIPanelButtonTemplate")
									end
									v.cancelclassButton:SetText("Cancel")
									v.cancelclassButton:SetWidth(61)
									v.cancelclassButton:SetFrameLevel(100)
									v.cancelclassButton:SetPoint('TOPLEFT', f,'TOPLEFT', p1xoffset+54, p1yoffset-18)
									v.cancelclassButton:Show()
									v.cancelclassButton:SetScript("OnClick",function(self)
										PlaySoundFile([[Sound\Interface\Uchatscrollbutton.ogg]],"SFX");
										changeslocked.state=false
										v[pname.."frame"]:Show()
										for i=1, 8 do
											v["grp"..i.."frame"]:Show()
										end	
										v.addplayerBox:Hide()
										v.addplayerBox:SetText("")
										v.classButton:Hide()
										v.cancelclassButton:Hide()
										v.okButton:Hide()
									end)
						--//
						--ADD PLAYER: OK BUTTON
									if not v.okButton then
										v.okButton = CreateFrame("Button", RM.ver.."_OkButton", f, "UIPanelButtonTemplate")
									end
									v.okButton:SetText("Ok")
									v.okButton:SetWidth(28)
									v.okButton:SetFrameLevel(100)
									v.okButton:SetPoint('TOPLEFT', f,'TOPLEFT', p1xoffset+108, p1yoffset+2)
									v.okButton:Show()
									v.okButton:SetScript("OnClick",function(self)
										PlaySoundFile([[Interface\Addons\RaidMover\Sounds\RingDing.ogg]],"SFX");
										changeslocked.state=false
										v[pname.."frame"]:ClearAllPoints()
										playerchanges.are=true
										playerchanges[v.addplayerBox:GetText()] = {}
										playerchanges[v.addplayerBox:GetText()].subgrp = templayout[pname].subgrp
										playerchanges[v.addplayerBox:GetText()].class = classpicked
										if pDB.layouts[layoutselected] then
											RM:SavesDisplay(layoutselected)
										else
											RM:SavesDisplay(RM:deepCopy(templayout, {}))
										end
									end)
						--//
						--ADD PLAYER: "ON ENTER" ADD PLAYER TEXT BOX FUNCTIONALITY
									v.addplayerBox:SetScript("OnEscapePressed",function(self)
										self:ClearFocus();
									end)
									v.addplayerBox:SetScript("OnEnterPressed",function(self)
										PlaySoundFile([[Interface\Addons\RaidMover\Sounds\RingDing.ogg]],"SFX");
										changeslocked.state=false
										v[pname.."frame"]:ClearAllPoints()
										playerchanges.are=true
										playerchanges[v.addplayerBox:GetText()] = {}
										playerchanges[v.addplayerBox:GetText()].subgrp = templayout[pname].subgrp
										playerchanges[v.addplayerBox:GetText()].class = classpicked
										if pDB.layouts[layoutselected] then
											RM:SavesDisplay(layoutselected)
										else
											RM:SavesDisplay(RM:deepCopy(templayout, {}))
										end
									end)
									--//
								end},
								--//END ADD PLAYER
								{ text="Add From Raid", hasArrow = true, notCheckable=true, menuList=addfromraid },
								--//END ADD PLAYER FROM RAID
								
								{ text="Cancel" , notCheckable=true,},
							}
							--//END 'addplayermenu' table
							local addplayermenuFrame = CreateFrame("Frame", RM.ver.."_AddPlayerMenuFrame", UIParent, "UIDropDownMenuTemplate")
							EasyMenu(addplayermenu, addplayermenuFrame, "cursor", 0 , 0, "MENU", 0.5);
						else
							RM:Print(changeslocked[changeslocked.state])
						end
	--REMOVE/EDIT PLAYER RIGHT CLICK MENU
					else
						if changeslocked.state==false then
							local copytoList = RM:CopyToMenu(pname)
							local editplayermenu = {
								{ text = "Remove "..pname, notCheckable=true, func = function()
									PlaySoundFile([[Interface\Addons\RaidMover\Sounds\Flame1.ogg]],"SFX"); 
									v[pname.."frame"]:Hide();
									v[pname.."frame"]:ClearAllPoints();
									playerchanges[pname] = true
									playerchanges.are=true
									if pDB.layouts[layoutselected] then
										RM:SavesDisplay(layoutselected)
									else
										RM:SavesDisplay(RM:deepCopy(templayout, {}))
									end
								end},
								--//END REMOVE PLAYER TEXT
						--EDIT PLAYER: EDIT BOX
								{ text = "Edit "..pname, notCheckable=true, func = function()
									PlaySoundFile([[Sound\Interface\Uescapescreenopen.ogg]],"SFX");
									changeslocked.state="editingplayer"
									local _,_,_,p1xoffset,p1yoffset = v[pname.."frame"]:GetPoint(1)
									p1xoffset=math.floor(p1xoffset)
									p1yoffset=math.floor(p1yoffset)
									if not v.addplayerBox then
										v.addplayerBox = CreateFrame("EditBox", RM.ver.."_"..pname.."AddPlayerBox", f, "InputBoxTemplate")
									end
									v.addplayerBox:SetText(pname)
									v.addplayerBox:SetWidth(110)
									v.addplayerBox:SetHeight(18)
									v.addplayerBox:SetFrameLevel(100)
									v.addplayerBox:SetCursorPosition(0)
									v.addplayerBox:SetPoint('TOPLEFT', f,'TOPLEFT', p1xoffset+3, p1yoffset)
									v.addplayerBox:SetAutoFocus(false);
									v.addplayerBox:Show()
									v[pname.."frame"]:Hide();
									for i=1, 8 do
										v["grp"..i.."frame"]:Hide()
									end	
						--//
						--EDIT PLAYER: CLASS BUTTON DROPDOWN MENU
									if not v.classButton then
										v.classButton = CreateFrame("Button", RM.ver.."_ClassButton", f, "UIPanelButtonTemplate")
									end
									if templayout[pname].class=="Death Knight" then
										v.classButton:SetText(RM.classcolor[templayout[pname].class].code.."DKnight");
									else
										v.classButton:SetText(RM.classcolor[templayout[pname].class].code..templayout[pname].class)
									end
									classpicked=templayout[pname].class
									v.classButton:SetWidth(61)
									v.classButton:SetFrameLevel(100)
									v.classButton:SetPoint('TOPLEFT', f,'TOPLEFT', p1xoffset-4, p1yoffset-18)
									v.classButton:Show()
									v.classButton:SetScript("OnClick",function(self)
										RM:ClassMenu()
									end)
						--//
						--EDIT PLAYER: CANCEL BUTTON
									if not v.cancelclassButton then
										v.cancelclassButton = CreateFrame("Button", RM.ver.."_CancelClassButton", f, "UIPanelButtonTemplate")
									end
									v.cancelclassButton:SetText("Cancel")
									v.cancelclassButton:SetWidth(61)
									v.cancelclassButton:SetFrameLevel(100)
									v.cancelclassButton:SetPoint('TOPLEFT', f,'TOPLEFT', p1xoffset+54, p1yoffset-18)
									v.cancelclassButton:Show()
									v.cancelclassButton:SetScript("OnClick",function(self)
										PlaySoundFile([[Sound\Interface\Uchatscrollbutton.ogg]],"SFX");
										changeslocked.state=false
										v[pname.."frame"]:Show()
										for i=1, 8 do
											v["grp"..i.."frame"]:Show()
										end	
										v.addplayerBox:Hide()
										v.addplayerBox:SetText("")
										v.classButton:Hide()
										v.cancelclassButton:Hide()
										v.okButton:Hide()
									end)
						--//
						--EDIT PLAYER: OK BUTTON
									if not v.okButton then
										v.okButton = CreateFrame("Button", RM.ver.."_OkButton", f, "UIPanelButtonTemplate")
									end
									v.okButton:SetText("Ok")
									v.okButton:SetWidth(28)
									v.okButton:SetFrameLevel(100)
									v.okButton:SetPoint('TOPLEFT', f,'TOPLEFT', p1xoffset+108, p1yoffset+2)
									v.okButton:Show()
									v.okButton:SetScript("OnClick",function(self)
										PlaySoundFile([[Interface\Addons\RaidMover\Sounds\RingDing.ogg]],"SFX");
										changeslocked.state=false
										--NO CHANGE
										if v.addplayerBox:GetText()==pname and classpicked==templayout[pname].class then
											changeslocked.state=false
											v[pname.."frame"]:Show()
											v.addplayerBox:Hide()
											v.addplayerBox:SetText("")
											v.classButton:Hide()
											v.cancelclassButton:Hide()
											v.okButton:Hide()
										--NAME OR CLASS CHANGE
										else
											v[pname.."frame"]:ClearAllPoints()
											v[pname..templayout[pname].class.."Texture"]:Hide()
											v[pname..templayout[pname].class.."Texture"]:ClearAllPoints()
											v[pname..templayout[pname].class.."HighlightTexture1"]:Hide()
											v[pname..templayout[pname].class.."HighlightTexture1"]:ClearAllPoints()
											playerchanges.are=true
											playerchanges[v.addplayerBox:GetText()] = {}
											playerchanges[v.addplayerBox:GetText()].subgrp = templayout[pname].subgrp
											playerchanges[v.addplayerBox:GetText()].class = classpicked
											if v.addplayerBox:GetText()~=pname then
												playerchanges[pname] = true
											end
											if pDB.layouts[layoutselected] then
												RM:SavesDisplay(layoutselected)
											else
												RM:SavesDisplay(RM:deepCopy(templayout, {}))
											end
										end
									end)
						
						--//
						--EDIT PLAYER: "ON ENTER" ADD PLAYER TEXT BOX FUNCTIONALITY
									v.addplayerBox:SetScript("OnEscapePressed",function(self)
										self:ClearFocus();
									end)
									v.addplayerBox:SetScript("OnEnterPressed",function(self)
										PlaySoundFile([[Interface\Addons\RaidMover\Sounds\RingDing.ogg]],"SFX");
										changeslocked.state=false
										--NO CHANGE
										if v.addplayerBox:GetText()==pname and classpicked==templayout[pname].class then
											changeslocked.state=false
											v[pname.."frame"]:Show()
											v.addplayerBox:Hide()
											v.addplayerBox:SetText("")
											v.classButton:Hide()
											v.cancelclassButton:Hide()
											v.okButton:Hide()
										--NAME OR CLASS CHANGE
										else
											v[pname.."frame"]:ClearAllPoints()
											v[pname..templayout[pname].class.."Texture"]:Hide()
											v[pname..templayout[pname].class.."Texture"]:ClearAllPoints()
											v[pname..templayout[pname].class.."HighlightTexture1"]:Hide()
											v[pname..templayout[pname].class.."HighlightTexture1"]:ClearAllPoints()
											playerchanges.are=true
											playerchanges[v.addplayerBox:GetText()] = {}
											playerchanges[v.addplayerBox:GetText()].subgrp = templayout[pname].subgrp
											playerchanges[v.addplayerBox:GetText()].class = classpicked
											if v.addplayerBox:GetText()~=pname then
												playerchanges[pname] = true
											end
											if pDB.layouts[layoutselected] then
												RM:SavesDisplay(layoutselected)
											else
												RM:SavesDisplay(RM:deepCopy(templayout, {}))
											end
										end
									end)
								end},
								--//END EDIT PLAYER TEXT
						--EDIT PLAYER: COPY TO NESTED MENU
								{ text="Copy "..pname.." to...", notCheckable=true, hasArrow=true, menuList=copytoList},
								--//END COPY TO MENU
								{ text="Cancel", notCheckable=true,},
							}
							local editplayermenuFrame = CreateFrame("Frame", RM.ver.."_EditPlayerMenuFrame", UIParent, "UIDropDownMenuTemplate")
							EasyMenu(editplayermenu, editplayermenuFrame, "cursor", 0 , 0, "MENU", 0.5);
						else
							RM:Print(changeslocked[changeslocked.state])
						end
					end
	--LEFT CLICK PLAYER FUNCTIONALITY
				else
					if changeslocked.state==false or changeslocked.state=="playerswap" then
						if not v[pname.."frame"].selected then
							if not swapplayer then
								PlaySoundFile([[Sound\Interface\Pickup\Pickupgems.ogg]],"SFX");
								v[pname.."frame"].selected=true
								v[pname..templayout[pname].class.."HighlightTexture1"]:SetDrawLayer("OVERLAY")
								v[pname.."HighlightTexture2"]:SetDrawLayer("OVERLAY")
								v[pname.."HighlightTexture3"]:SetDrawLayer("OVERLAY")
								swapplayer = pname
								changeslocked.state="playerswap"
								for i=1, 8 do
									v["grp"..i.."frame"]:Hide()
								end	
							else
								for i=1, 8 do
									v["grp"..i.."frame"]:Show()
								end	
								PlaySoundFile([[Sound\Interface\Pickup\Putdowngems.ogg]],"SFX");
								RM:PlayerSwap(swapplayer, pname)
								changeslocked.state=false
							end
						else
							for i=1, 8 do
								v["grp"..i.."frame"]:Show()
							end	
							PlaySoundFile([[Sound\Interface\Pickup\Putdowngems.ogg]],"SFX");
							v[pname..templayout[pname].class.."HighlightTexture1"]:SetDrawLayer("HIGHLIGHT")
							v[pname.."HighlightTexture2"]:SetDrawLayer("HIGHLIGHT")
							v[pname.."HighlightTexture3"]:SetDrawLayer("HIGHLIGHT")
							v[pname.."frame"].selected=false
							changeslocked.state=false
							swapplayer = nil
						end
					else
						RM:Print(changeslocked[changeslocked.state])
					end					
				end
			end);

----PLAYERNAME TEXT AND STATIC CLASS ICON
			if not emptyslots[pname] then
				if not v[pname.."string"] then
					v[pname.."string"]=v[pname.."frame"]:CreateFontString(RM.ver.."_"..pname.."Text","ARTWORK","GameFontNormal")
				end
				if string.find(pname, "-") then
					v[pname.."string"]:SetText(string.sub(pname,1,string.find(pname, "-")-1));
				else
					v[pname.."string"]:SetText(pname);
				end
				v[pname.."string"]:GetWidth()
				v[pname.."string"]:SetFont([[Fonts\ARIALN.ttf]],16);
				v[pname.."string"]:SetTextColor(RM.classcolor[class].r, RM.classcolor[class].g, RM.classcolor[class].b)
				v[pname.."string"]:SetPoint("CENTER",v[pname.."frame"],"CENTER",((v[pname.."string"]:GetWidth()/2)-38),1)
				if not v[pname..class.."Texture"] then
					v[pname..class.."Texture"]=gs.CreateTexture(v[pname.."frame"],self.ver.."_"..pname..class.."Texture","ARTWORK",18,36,"CENTER", v[pname.."frame"], "CENTER", -47, 8,classtexturepath[class]);
				else
					v[pname..class.."Texture"]:Show()
					v[pname..class.."Texture"]:SetPoint("CENTER",v[pname.."frame"],"CENTER",-47,8)
				end
			end

----CLASS ICON HIGHLIGHT 'POPOUT' TEXTURES
			if not v[pname..class.."HighlightTexture1"] then			
				v[pname..class.."HighlightTexture1"]=gs.CreateTexture(v[pname.."frame"],self.ver.."_"..pname..class.."HighlightTexture1","HIGHLIGHT",26,48,"CENTER", v[pname.."frame"], "CENTER", -46, 11,classtexturepath[class]);
			else
				v[pname..class.."HighlightTexture1"]:Show()
				v[pname..class.."HighlightTexture1"]:SetPoint("CENTER",v[pname.."frame"],"CENTER",-46,11)
			end
			
			if not v[pname.."HighlightTexture2"] then
				v[pname.."HighlightTexture2"]=gs.CreateTexture(v[pname.."frame"],self.ver.."_"..pname.."HighlightTexture2","HIGHLIGHT",26,47,"CENTER", v[pname.."frame"], "CENTER", -46, 9,[[Interface\BUTTONS\UI-MicroButton-Hilight]]);
				v[pname.."HighlightTexture3"]=gs.CreateTexture(v[pname.."frame"],self.ver.."_"..pname.."HighlightTexture3","HIGHLIGHT",27,48,"CENTER", v[pname.."frame"], "CENTER", -46, 10,[[Interface\BUTTONS\UI-MicroButton-Hilight]]);
			end
		end

----GROUP# Frame Creation
		points.y = {-78, -78, -78, -178, -178, -178, -278, -278, -278}
		for i=1, 8 do
			if not v["grp"..i.."frame"] then
				v["grp"..i.."frame"]=CreateFrame("Frame","grp"..i.."frame",f);
			end	
			v["grp"..i.."frame"]:SetWidth(15);
			v["grp"..i.."frame"]:SetHeight(20);
			v["grp"..i.."frame"]:Show();
			v["grp"..i.."frame"]:EnableMouse(true);
			v["grp"..i.."frame"]:SetMovable(false);
			v["grp"..i.."frame"]:SetResizable(false);
			v["grp"..i.."frame"]:SetPoint('TOPLEFT',f,'TOPLEFT',(points.x[i])+115, points.y[i])
			v["grp"..i.."frame"].selected=false;
----GROUP# OnMouseDown SELECTION
			v["grp"..i.."frame"]:SetScript("OnMouseDown",function()
				if changeslocked.state==false or changeslocked.state=="groupswap"then
					if not v["grp"..i.."frame"].selected then
						if not swapgroup then
							PlaySoundFile([[Sound\Interface\Pickup\Pickupmetallarge.ogg]],"SFX");
							v["grp"..i.."frame"].selected=true
							for pname, ptable in pairs(templayout) do
								if templayout[pname].subgrp==i then
									v[pname..templayout[pname].class.."HighlightTexture1"]:SetDrawLayer("OVERLAY")
									v[pname.."HighlightTexture2"]:SetDrawLayer("OVERLAY")
								end
							end
							v["grp"..i.."HighlightTexture2"]:SetDrawLayer("OVERLAY")
							swapgroup = i
							changeslocked.state="groupswap"
						else
							PlaySoundFile([[Sound\Interface\Pickup\Putdownlargemetal.ogg]],"SFX");
							v["grp"..i.."HighlightTexture2"]:SetDrawLayer("HIGHLIGHT")
							self:GroupSwap(swapgroup, i)
							changeslocked.state=false
						end
					else
						for pname, ptable in pairs(templayout) do
							if templayout[pname].subgrp==i then
								v[pname..templayout[pname].class.."HighlightTexture1"]:SetDrawLayer("HIGHLIGHT")
								v[pname.."HighlightTexture2"]:SetDrawLayer("HIGHLIGHT")
							end
						end
						PlaySoundFile([[Sound\Interface\Pickup\Putdownlargemetal.ogg]],"SFX");
						v["grp"..i.."HighlightTexture2"]:SetDrawLayer("HIGHLIGHT")
						v["grp"..i.."frame"].selected=false
						changeslocked.state=false
						swapgroup = nil
					end
				else
					RM:Print(changeslocked[changeslocked.state])
				end
			end);
----GROUP# OnEnter
			v["grp"..i.."frame"]:SetScript("OnEnter",function()
				if changeslocked.state==false or changeslocked.state=="groupswap" then
					for pname, ptable in pairs(templayout) do
						if templayout[pname].subgrp==i then
							v[pname..templayout[pname].class.."HighlightTexture1"]:SetDrawLayer("OVERLAY")
							v[pname.."HighlightTexture2"]:SetDrawLayer("OVERLAY")
						end
					end
				end	
			end);
----GROUP# OnLeave			
			v["grp"..i.."frame"]:SetScript("OnLeave",function()
				if changeslocked.state==false or changeslocked.state=="groupswap" then
					for pname, ptable in pairs(templayout) do
						if templayout[pname].subgrp==i and v["grp"..i.."frame"].selected==false then
							v[pname..templayout[pname].class.."HighlightTexture1"]:SetDrawLayer("HIGHLIGHT")
							v[pname.."HighlightTexture2"]:SetDrawLayer("HIGHLIGHT")
						end
					end
				end	
			end);
			
----GROUP HIGHLIGHT TEXTURES
			if not v["grp"..i.."HighlightTexture2"] then
				--HIGHLIGHT2 is a YELLOW BORDER
				v["grp"..i.."HighlightTexture2"]=gs.CreateTexture(v["grp"..i.."frame"],self.ver.."_".."grp"..i.."HighlightTexture3","HIGHLIGHT",142,105,"CENTER", v["grp"..i.."frame"], "CENTER", -59, -34,[[Interface\ContainerFrame\UI-Icon-QuestBorder]]);
			end
		end
	end	

--PLAYERSWAP
	function self:PlayerSwap(p1, p2)
		local p1subgrp, p2subgrp = templayout[p1].subgrp, templayout[p2].subgrp
		if p1subgrp~=p2subgrp then
			if not string.find(v.title:GetText(), "*") then
				v.title:SetText(v.title:GetText().."*")
			end
			templayout[p1].subgrp = p2subgrp
			templayout[p2].subgrp = p1subgrp
			playerchanges[p1] = {} 
			playerchanges[p2] = {}
			playerchanges[p1].subgrp = p2subgrp
			playerchanges[p1].class = templayout[p1].class
			playerchanges[p2].subgrp = p1subgrp
			playerchanges[p2].class = templayout[p2].class
			playerchanges.are=true
		end
		
		local _,_,_,p1xoffset,p1yoffset = v[p1.."frame"]:GetPoint(1)
		local _,_,_,p2xoffset,p2yoffset = v[p2.."frame"]:GetPoint(1)
		v[p1.."frame"]:SetPoint('TOPLEFT',f,'TOPLEFT',p2xoffset, p2yoffset)
		v[p2.."frame"]:SetPoint('TOPLEFT',f,'TOPLEFT',p1xoffset, p1yoffset)
		v[p1..templayout[p1].class.."HighlightTexture1"]:SetDrawLayer("HIGHLIGHT")
		v[p2..templayout[p2].class.."HighlightTexture1"]:SetDrawLayer("HIGHLIGHT")
		v[p1.."HighlightTexture2"]:SetDrawLayer("HIGHLIGHT")
		v[p2.."HighlightTexture2"]:SetDrawLayer("HIGHLIGHT")
		v[p1.."HighlightTexture3"]:SetDrawLayer("HIGHLIGHT")
		v[p2.."HighlightTexture3"]:SetDrawLayer("HIGHLIGHT")
		v[p1.."frame"].selected=false;
		v[p2.."frame"].selected=false;
		swapplayer = nil
	end

--GROUPSWAP
	function self:GroupSwap(g1, g2)
		local lockedplayers = {}
		--get p1 and p2's subgroup then addplayer
		for p1, ptable1 in pairs(templayout) do
			if templayout[p1].subgrp == g1 and not lockedplayers[p1] then
				for p2, ptable2 in pairs(templayout) do
					if templayout[p2].subgrp == g2 and not lockedplayers[p2] and not lockedplayers[p1] then
						lockedplayers[p1], lockedplayers[p2] = true, true
						RM:PlayerSwap(p1, p2)
						v["grp"..g1.."HighlightTexture2"]:SetDrawLayer("HIGHLIGHT")
						v["grp"..g2.."HighlightTexture2"]:SetDrawLayer("HIGHLIGHT")
						v["grp"..g1.."frame"].selected=false;
						v["grp"..g2.."frame"].selected=false;
					end
				end
			end
		end
		swapgroup = nil
		lockedplayers = {}
	end

--"SAVES" DROPDOWN MENU
	function self:SavesMenu()
		local savesList = {}
		local i = 1
		for layoutname, layoutnames in pairs(pDB.layoutlist) do
			savesList[i] = { text = layoutnames, notCheckable=true, func = function() 
				playerchanges = {}; 
				PlaySoundFile([[Interface\Addons\RaidMover\Sounds\AddSocketQuest.ogg]],"SFX"); 
				RM:SavesDisplay(layoutnames); 
			end }
			i = i+1
		end
		
		local savesmenuFrame = CreateFrame("Frame", "SavesDropdownMenuFrame", UIParent, "UIDropDownMenuTemplate")
		EasyMenu(savesList, savesmenuFrame, v.savesButton, 0 , 2, "MENU", 0.5);
	end
	
	function self:CopyToMenu(pname)
		local copytoList={}
		local iter = 1
		local cantmove=true
		--local opengrp=nil
		--local playerspergrp={}
		for layoutname, layoutnames in pairs(pDB.layoutlist) do
			--RM:Print(layoutnames)
			if pDB.layouts[layoutnames][pname] then
				cantmove=true
			else
				cantmove=false
			end
			
			copytoList[iter] = 
			{	text=layoutnames, disabled=cantmove, notCheckable=true, func=function()
					local playerspergrp=RM:getPlayersPerGrp(layoutnames)
					local opengrp=nil
					for i=1,8 do
						--RM:Print("Grp"..i..": "..playerspergrp[i])
						if playerspergrp[i]<5 then
							--RM:Print("Can move to grp"..i)
							opengrp=i
							cantmove=false
							break
						end
					end
					pDB.layouts[layoutnames][pname]={}
					pDB.layouts[layoutnames][pname].subgrp=opengrp
					pDB.layouts[layoutnames][pname].class=templayout[pname].class
					PlaySoundFile([[Interface\Addons\RaidMover\Sounds\RingDing.ogg]],"SFX");
					RM:Print("Copied "..pname.." to group "..opengrp.." in "..layoutnames)
				end
			}
			iter = iter+1
		end
		return copytoList
	end

--CLASS DROPDOWN MENU
	function self:ClassMenu()
		local classList = {}
		local i = 1
		for class, color in pairs(RM.classcolor) do
			classList[i] = {text = class, colorCode=color.code, notCheckable=true, func = function() 
				classpicked=class;
				if class=="Death Knight" then
					v.classButton:SetText(RM.classcolor[class].code.."DKnight");
				else
					v.classButton:SetText(RM.classcolor[class].code..class);
				end
			end}
			i = i+1
		end
		local classmenuFrame = CreateFrame("Frame", "ClassDropdownMenuFrame", UIParent, "UIDropDownMenuTemplate")
		EasyMenu(classList, classmenuFrame, "cursor", 0 , 0, "MENU", 0.5);
	end
	
--WIPE	
	function self:WipeList()
		changeslocked.state=false
		v.savetextBox:Hide();
		v.newsaveButton:Hide()
		v.overwriteButton:Hide()
		v.cancelButton:Hide()
		v.restoreButton:Hide()
		v.saveButton:Hide()
		v.clearButton:Hide()
		v.deleteButton:Hide()
		v.renameButton:Hide()
		v.inviteButton:Hide()
		v.shareButton:Hide()
		if v.delnoButton then
			v.delnoButton:Hide()
			v.delyesButton:Hide()
			v.deleteText:Hide()
		end
		if v.clearnoButton then
			v.clearnoButton:Hide()
			v.clearyesButton:Hide()
			v.clearText:Hide()
		end
		if v.renamenoButton then
			v.renamenoButton:Hide()
			v.renameyesButton:Hide()
			v.renametextBox:Hide()
		end
		
		if templayout then
			for pname, ptable in pairs(templayout) do
				if pname and v[pname.."frame"] then
					v[pname.."frame"]:Hide();
					v[pname.."frame"]:ClearAllPoints()
				end
			end
			wipe(templayout)
			templayout=nil
		end
		
		if v["grp1frame"] then
			for i=1, 8 do
				v["grp"..i.."frame"]:Hide();
				v["grp"..i.."frame"]:ClearAllPoints()
			end
		end
		
		if v.addplayerBox then
			v.addplayerBox:Hide()
			v.addplayerBox:SetText("")
			v.classButton:Hide()
			v.cancelclassButton:Hide()
			v.okButton:Hide()
		end
		
		if swapplayer then
			v[swapplayer..templayout[swapplayer].class.."HighlightTexture1"]:SetDrawLayer("HIGHLIGHT")
			v[swapplayer.."HighlightTexture2"]:SetDrawLayer("HIGHLIGHT")
			v[swapplayer.."HighlightTexture3"]:SetDrawLayer("HIGHLIGHT")
			v[swapplayer.."frame"].selected=false;
			swapplayer = nil
		end
		
		if swapgroup then
			v["grp"..swapgroup.."frame"].selected=false;
			swapgroup = nil
		end

		if v.renametextBox then
			v.renametextBox:Hide()
			v.renametextBox:SetText("")
		end
		
		if not v.maintabtexture:IsShown() then
			v.raidButton:Show()
			v.blankButton:Show()
		end
		layoutselected = nil
		collectgarbage()
	end

--EMPTYSLOTS - CREATE "FAKE" PLAYERS
	function self:EmptySlots(cmd, layout)
		if cmd == "add" then
			RM:EmptySlots("remove",templayout)
			--ADDS EMPTY PLAYERS TO THE TEMPLAYOUT TO CREATE BLANK BUTTONS FOR SWAPS
			local playerspergrp = RM:getPlayersPerGrp(templayout)
			for grp, grpcount in pairs(playerspergrp) do
				if grpcount < 5 then
					for i=1, 5-grpcount do
						if not templayout[grp..i] then
							templayout[grp..i] = {}
							templayout[grp..i].subgrp = grp
							templayout[grp..i].class = "NA"
						end
					end
				end
			end
		elseif cmd == "remove" then
			for pname, ptable in pairs(layout) do
				if emptyslots[pname] then
					layout[pname] = nil;
				end
			end
			return layout
		elseif cmd == "blank" then
			for grp=1, 8 do
				for i=1, 5 do
					layout[grp..i] = {}
					layout[grp..i].subgrp = grp
					layout[grp..i].class = "NA"
				end
			end
			return layout
		end
	end

end

function RM:CreateOptions()
	local buttonsY=-226
	local thresholds={[1]={["qual"]="Common", ["color"]="WHITE"}, [2]={["qual"]="Uncommon", ["color"]="UNCOMMON"}, [3]={["qual"]="Rare", ["color"]="RARE"}, [4]={["qual"]="Epic", ["color"]="EPIC"}}
	local difficulties={[3]=RM.color["WHITE"].."10 Player|r",[4]=RM.color["WHITE"].."25 Player|r",[5]=RM.color["WHITE"].."10 Player|r"..RM.color["CRIMSON"].."(Heroic)|r",[6]=RM.color["WHITE"].."25 Player|r"..RM.color["CRIMSON"].."(Heroic)|r"}
	
	if not v.autoattend then
		--AUTO ATTENDANCE ENABLE/DISABLE
		v.autoattend=CreateFrame("CheckButton",RM.ver.."_AutoAttendFrame",RM.mainFrame , "UICheckButtonTemplate");
		v.autoattend:SetPoint("TOPLEFT",RM.mainFrame, "TOPLEFT", 15, -70)
		_G[v.autoattend:GetName() .. "Text"]:SetText("Attendance Tracking on Boss Kill")
		
		--AUTO PROMOTIONS
		v.autopromote=CreateFrame("CheckButton",RM.ver.."_AutoPromoteFrame",RM.mainFrame , "UICheckButtonTemplate");
		v.autopromote:SetPoint("TOPLEFT",RM.mainFrame, "TOPLEFT", 15, -95)
		_G[v.autopromote:GetName() .. "Text"]:SetText("Raid Assist Promotions")
		if not v.promoteBox then
			v.promoteBox = CreateFrame("EditBox", RM.ver.."_PromoteBox", v.autopromote, "InputBoxTemplate")
		end
		v.promoteBox:SetText("")
		v.promoteBox:SetWidth(110)
		v.promoteBox:SetHeight(18)
		v.promoteBox:SetCursorPosition(0)
		v.promoteBox:SetPoint("CENTER", v.autopromote,"RIGHT", 196, 1)
		v.promoteBox:Show()
		v.promoteBox:SetAutoFocus(false)
		v.promoteBox:SetScript("OnEnterPressed", function()
			if v.promoteBox:GetText()~=nil or v.promoteBox:GetText()~="" then
				table.insert(pDB.auto.promotelist, v.promoteBox:GetText())
				RM:Print("Added "..v.promoteBox:GetText().." to auto promote list")
				table.sort(pDB.auto.promotelist)
				v.promoteBox:SetText("")
				v.promoteBox:ClearFocus()
			end
		end)
		v.promoteBox:SetScript("OnEscapePressed", function()
			v.promoteBox:SetText("")
			v.promoteBox:ClearFocus()
		end)
		--PROMOTE ADD BUTTON
		v.promoteaddButton=CreateFrame("Button", RM.ver.."_PromoteAddButton", RM.mainFrame, "UIPanelButtonTemplate")
		v.promoteaddButton:SetText("Add")
		v.promoteaddButton:SetPoint("CENTER", v.autopromote,"RIGHT", 268, 1);
		v.promoteaddButton:SetWidth(33)
		v.promoteaddButton:SetScript("OnClick",function(self)
			if v.promoteBox:GetText()==nil or v.promoteBox:GetText()=="" then
				RM:Print("There is no name to add.")
			else	
				table.insert(pDB.auto.promotelist, v.promoteBox:GetText())
				RM:Print("Added "..v.promoteBox:GetText().." to auto promote list")
				table.sort(pDB.auto.promotelist)
			end
			v.promoteBox:SetText("")
			v.promoteBox:ClearFocus()
		end)
		--PROMOTE PRINT BUTTON
		v.promoteprintButton=CreateFrame("Button", RM.ver.."_PromotePrintButton", RM.mainFrame, "UIPanelButtonTemplate")
		v.promoteprintButton:SetText("Print")
		v.promoteprintButton:SetPoint("CENTER", v.autopromote,"RIGHT", 354, 1);
		v.promoteprintButton:SetWidth(40)
		v.promoteprintButton:SetScript("OnClick",function(self)
			local printstr=""
			for i=1, #pDB.auto.promotelist do
				if pDB.auto.promotelist[i]==nil or pDB.auto.promotelist[i]=="" then
					table.remove(pDB.auto.promotelist,i)
				else
					printstr=printstr..pDB.auto.promotelist[i]..", "
				end
			end
			RM:Print(printstr)
		end)
		v.promoteprintButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"promotelistprint") end)
		v.promoteprintButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
		--PROMOTE REMOVE BUTTON
		v.promoteremoveButton=CreateFrame("Button", RM.ver.."_PromoteRemoveButton", RM.mainFrame, "UIPanelButtonTemplate")
		v.promoteremoveButton:SetText("Remove")
		v.promoteremoveButton:SetPoint("CENTER", v.autopromote,"RIGHT", 310, 1);
		v.promoteremoveButton:SetWidth(52)
		v.promoteremoveButton:SetScript("OnClick",function(self)
			for i=1, #pDB.auto.promotelist do
				if pDB.auto.promotelist[i]==v.promoteBox:GetText() then
					table.remove(pDB.auto.promotelist,i)
					RM:Print(v.promoteBox:GetText().." has been removed")
					table.sort(pDB.auto.promotelist)
				end
			end
			v.promoteBox:SetText("")
			v.promoteBox:ClearFocus()
		end)
		
		
		--AUTO RAID CONVERSION AND DIFFICULTY
		v.convertraidframe=CreateFrame("CheckButton",RM.ver.."_AutoRaidConvertFrame",RM.mainFrame , "UICheckButtonTemplate");
		v.convertraidframe:SetPoint("TOPLEFT",RM.mainFrame, "TOPLEFT", 15, -120)
		_G[v.convertraidframe:GetName() .. "Text"]:SetText("Convert to Raid: ")
				
		--RAID DIFFICULTY TEXT--
		v.difficultyframe=CreateFrame("Frame",RM.ver.."_DifficultyFrame",RM.mainFrame);
		v.difficultyframe:SetWidth(70);
		v.difficultyframe:SetHeight(18);
		v.difficultyframe:EnableMouse(true);
		v.difficultyframe:SetMovable(false);
		v.difficultyframe:SetResizable(false);
		v.difficultyframe:SetPoint("LEFT",v.convertraidframe,"RIGHT",105,0)
		v.difficultyframe:SetScript("OnEnter", function(self, button)
			v.difficultytext:SetFont([[Fonts\ARIALN.ttf]],19);
			RM:OnEnter(self, "difficulty")
		end)
		v.difficultyframe:SetScript("OnLeave", function(self, button)
			v.difficultytext:SetFont([[Fonts\ARIALN.ttf]],16);
			RM:OnLeave(self)
		end)
		v.difficultyframe:SetScript("OnMouseDown", function(self, button)
			local difficultylist = {}
			for i=1,4 do
				difficultylist[i] = { text = difficulties[i+2], notCheckable=true, func = function()
					pDB.auto.difficulty=i+2
					v.difficultytext:SetText(difficulties[i+2]);
					v.difficultyframe:SetWidth(v.difficultytext:GetWidth());
				end}
			end
		
			local difficultymenuFrame = CreateFrame("Frame", "DifficultyMenuFrame", UIParent, "UIDropDownMenuTemplate")
			EasyMenu(difficultylist, difficultymenuFrame, v.difficultyframe, 0 , 2, "MENU", 0.5);
		end)
		
		v.difficultytext=v.difficultyframe:CreateFontString(RM.ver.."_DifficultyText","ARTWORK","GameFontNormal")
		v.difficultytext:SetText(difficulties[pDB.auto.difficulty]);
		v.difficultytext:SetFont([[Fonts\ARIALN.ttf]],16);
		v.difficultytext:SetPoint("CENTER",v.difficultyframe,"CENTER",0,0)
		v.difficultyframe:SetWidth(v.difficultytext:GetWidth());
		
		--MASTER LOOT AND THRESHOLD
		v.automastloot=CreateFrame("CheckButton",RM.ver.."_AutoMastLootFrame",RM.mainFrame , "UICheckButtonTemplate");
		v.automastloot:SetPoint("TOPLEFT",RM.mainFrame, "TOPLEFT", 15, -145)
		_G[v.automastloot:GetName() .. "Text"]:SetText("Masterlooting set to:")
		
		--MASTERLOOTER NAME TEXT--
		v.mastlootframe=CreateFrame("Frame",RM.ver.."_MasterLooter",RM.mainFrame);
		v.mastlootframe:SetWidth(70);
		v.mastlootframe:SetHeight(18);
		v.mastlootframe:EnableMouse(true);
		v.mastlootframe:SetMovable(false);
		v.mastlootframe:SetResizable(false);
		v.mastlootframe:SetPoint("LEFT",v.automastloot,"RIGHT",126,0)
		v.mastlootframe:SetScript("OnEnter", function(self, button)
			v.mastlootertext:SetFont([[Fonts\ARIALN.ttf]],19);
			RM:OnEnter(self, "masterlooter")
		end)
		v.mastlootframe:SetScript("OnLeave", function(self, button)
			v.mastlootertext:SetFont([[Fonts\ARIALN.ttf]],16);
			RM:OnLeave(self)
		end)
		v.mastlootframe:SetScript("OnMouseDown", function(self, button)
			--EDITBOX with previous masterlooter name in it.
			if not v.mastlooterBox then
				v.mastlooterBox = CreateFrame("EditBox", RM.ver.."_MasterLooterBox", v.automastloot, "InputBoxTemplate")
			end
			v.thresholdframe:SetPoint("LEFT",v.mastlooterBox,"RIGHT",3,0)
			if pDB.auto.masterlooter then
				v.mastlooterBox:SetText(pDB.auto.masterlooter)
			else
				v.mastlooterBox:SetText(UnitName("player"));
			end
			v.mastlooterBox:SetWidth(80)
			v.mastlooterBox:SetHeight(18)
			v.mastlooterBox:SetCursorPosition(0)
			v.mastlooterBox:SetPoint("LEFT",v.automastloot,"RIGHT",126,0)
			v.mastlooterBox:Show()
			v.mastlootertext:Hide()
			v.mastlooterBox:SetAutoFocus(true)
			v.mastlooterBox:SetScript("OnEnterPressed", function(self)
				if v.mastlooterBox:GetText()==nil or v.mastlooterBox:GetText()=="" then
					pDB.auto.masterlooter=UnitName("player")
				else
					pDB.auto.masterlooter=v.mastlooterBox:GetText()
				end
				v.mastlootertext:SetText(RM.color["WHITE"]..pDB.auto.masterlooter.."|r,");
				v.mastlootframe:SetWidth(v.mastlootertext:GetWidth());
				v.mastlooterBox:ClearFocus()
				v.mastlooterBox:Hide()
				v.mastlootertext:Show()
				v.thresholdframe:SetPoint("LEFT",v.mastlootframe,"RIGHT",3,0)
			end)
			v.mastlooterBox:SetScript("OnEscapePressed", function()
				v.mastlooterBox:ClearFocus()
				v.mastlooterBox:Hide()
				v.mastlootertext:Show()
				v.thresholdframe:SetPoint("LEFT",v.mastlootframe,"RIGHT",3,0)
			end)
			v.mastlooterBox:SetScript("OnEnter", function(self, button) RM:OnEnter(self, "masterlooterbox") end)
			v.mastlooterBox:SetScript("OnLeave", function(self, button)	RM:OnLeave(self) end)
		end)
		
		v.mastlootertext=v.mastlootframe:CreateFontString(RM.ver.."_ThresholdText","ARTWORK","GameFontNormal")
		if pDB.auto.masterlooter then
			v.mastlootertext:SetText(RM.color["WHITE"]..pDB.auto.masterlooter.."|r,");
		else
			v.mastlootertext:SetText(RM.color["WHITE"]..UnitName("player").."|r,");
		end
		v.mastlootertext:SetFont([[Fonts\ARIALN.ttf]],16);
		v.mastlootertext:SetPoint("CENTER",v.mastlootframe,"CENTER",0,0)
		v.mastlootframe:SetWidth(v.mastlootertext:GetWidth());
		
		--THRESHOLD TEXT--
		v.thresholdframe=CreateFrame("Frame",RM.ver.."_MasterLootThreshold",RM.mainFrame);
		v.thresholdframe:SetWidth(70);
		v.thresholdframe:SetHeight(18);
		v.thresholdframe:EnableMouse(true);
		v.thresholdframe:SetMovable(false);
		v.thresholdframe:SetResizable(false);
		v.thresholdframe:SetPoint("LEFT",v.mastlootframe,"RIGHT",3,0)
		v.thresholdframe:SetScript("OnEnter", function(self, button)
			v.thresholdtext:SetFont([[Fonts\ARIALN.ttf]],19);
			RM:OnEnter(self, "threshold")
		end)
		v.thresholdframe:SetScript("OnLeave", function(self, button)
			v.thresholdtext:SetFont([[Fonts\ARIALN.ttf]],16);
			RM:OnLeave(self)
		end)
		v.thresholdframe:SetScript("OnMouseDown", function(self, button)
			local thresholdList = {}
			for i=1,4 do
				thresholdList[i] = { text = RM.color[thresholds[i].color]..thresholds[i].qual.."|r", notCheckable=true, func = function()
					pDB.auto.threshold=i
					v.thresholdtext:SetText(RM.color[thresholds[i].color]..thresholds[i].qual.."|r");
					v.thresholdframe:SetWidth(v.thresholdtext:GetWidth());
				end}
			end
		
			local thresholdmenuFrame = CreateFrame("Frame", "SavesDropdownMenuFrame", UIParent, "UIDropDownMenuTemplate")
			EasyMenu(thresholdList, thresholdmenuFrame, v.thresholdframe, 0 , 2, "MENU", 0.5);
		end)
		
		v.thresholdtext=v.thresholdframe:CreateFontString(RM.ver.."_ThresholdText","ARTWORK","GameFontNormal")
		if pDB.auto.threshold > 4 then
			pDB.auto.threshold=4
		end
		v.thresholdtext:SetText(RM.color[thresholds[pDB.auto.threshold].color]..thresholds[pDB.auto.threshold].qual.."|r")
		v.thresholdtext:SetFont([[Fonts\ARIALN.ttf]],16);
		v.thresholdtext:SetPoint("CENTER",v.thresholdframe,"CENTER",0,0)
		v.thresholdframe:SetWidth(v.thresholdtext:GetWidth());
		
		--DISABLE TOOLTIPS
		v.disabletooltips=CreateFrame("CheckButton",RM.ver.."_DisableToolTipsFrame",RM.mainFrame , "UICheckButtonTemplate");
		v.disabletooltips:SetPoint("TOPLEFT",RM.mainFrame, "TOPLEFT", 15, -170)
		_G[v.disabletooltips:GetName() .. "Text"]:SetText("Enable all Raid Mover tooltips")
		
		--DISABLE RECEIVING LAYOUTS
		v.disablelayouts=CreateFrame("CheckButton",RM.ver.."_DisableReceivingLayouts",RM.mainFrame , "UICheckButtonTemplate");
		v.disablelayouts:SetPoint("TOPLEFT",RM.mainFrame, "TOPLEFT", 15, -195)
		_G[v.disablelayouts:GetName() .. "Text"]:SetText("Receive layouts sent from other RaidMover's in raid")
		
		--EXPORT ATTENDANCE XML BUTTON
		v.attendanceExportXMLButton = CreateFrame("Button", RM.ver.."_AttendanceXMLexportButton", RM.mainFrame, "UIPanelButtonTemplate")
		v.attendanceExportXMLButton:SetText("Export Attendance XML")
		v.attendanceExportXMLButton:SetPoint("TOPLEFT",RM.mainFrame,"TOPLEFT",18,buttonsY);
		v.attendanceExportXMLButton:SetWidth(150)
		v.attendanceExportXMLButton:SetScript("OnClick",function(self)
			RM:PopupWindow(RM:exportAttendanceXML())
		  end)
		v.attendanceExportXMLButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"exportxml") end)
		v.attendanceExportXMLButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
		
		--EXPORT ATTENDANCE LUA BUTTON
		v.attendanceExportLUAButton = CreateFrame("Button", RM.ver.."_AttendanceLUAexportButton", RM.mainFrame, "UIPanelButtonTemplate")
		v.attendanceExportLUAButton:SetText("Export Attendance LUA")
		v.attendanceExportLUAButton:SetPoint("TOPLEFT",RM.mainFrame,"TOPLEFT",18,buttonsY-54);
		v.attendanceExportLUAButton:SetWidth(150)
		v.attendanceExportLUAButton:SetScript("OnClick",function(self)
			RM:PopupWindow(RM:exportAttendanceLUA())
		  end)
		v.attendanceExportLUAButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"exportlua") end)
		v.attendanceExportLUAButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
		
		--IMPORT ATTENDANCE LUA BUTTON
		v.attendanceImportButton = CreateFrame("Button", RM.ver.."_AttendanceLUAimportButton", RM.mainFrame, "UIPanelButtonTemplate")
		v.attendanceImportButton:SetText("Import Attendance LUA")
		v.attendanceImportButton:SetPoint("TOPLEFT",RM.mainFrame,"TOPLEFT",18,buttonsY-81);
		v.attendanceImportButton:SetWidth(150)
		v.attendanceImportButton:Disable()
		v.attendanceImportButton:SetScript("OnClick",function(self)
			RM:importAttendanceLUA("attendance")
		  end)
		v.attendanceImportButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"importlua") end)
		v.attendanceImportButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
		
		--IMPORT ATTENDANCE XML BUTTON
		v.attendanceImportButtonXML = CreateFrame("Button", RM.ver.."_AttendanceXMLimportButton", RM.mainFrame, "UIPanelButtonTemplate")
		v.attendanceImportButtonXML:SetText("Import Attendance XML")
		v.attendanceImportButtonXML:SetPoint("TOPLEFT",RM.mainFrame,"TOPLEFT",18,buttonsY-27);
		v.attendanceImportButtonXML:SetWidth(150)
		--v.attendanceImportButtonXML:Disable()
		v.attendanceImportButtonXML:SetScript("OnClick",function(self)
			RM:AttendanceImportPopoutXML()
		  end)
		v.attendanceImportButtonXML:SetScript("OnEnter", function(self) RM:OnEnter(self,"importxml") end)
		v.attendanceImportButtonXML:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
		
		--POPOUT BUTTON
		v.attendanceTestButton=CreateFrame("Button", RM.ver.."_AttendanceButtonPopup",RM.mainFrame,"UIPanelButtonTemplate")
		v.attendanceTestButton:SetPoint("TOPLEFT",RM.mainFrame,"TOPLEFT",18,buttonsY-108);
		v.attendanceTestButton:SetText("Popout")
		v.attendanceTestButton:SetWidth(58);
		v.attendanceTestButton:Disable()
		v.attendanceTestButton:SetScript("OnClick",function(self)
			if not v.attendancePopup then
				v.attendancePopup=CreateFrame("Frame", RM.ver.."_AttendancePop",UIParent)
				v.attendancePopup:EnableMouse(true);
				v.attendancePopup:SetMovable(true);
				v.attendancePopup:SetScript("OnMouseDown", function(self) self:StartMoving(); end)
				v.attendancePopup:SetScript("OnMouseUp", function(self) self:StopMovingOrSizing(); end)
				v.attendancePopup:SetFrameStrata("MEDIUM");
				
				v.attendanceCloseButton = CreateFrame("Button",RM.ver.."_AttendanceCloseButton", v.attendancePopup, "UIPanelCloseButton");
				
				--Background--
				v.attendanceTexture=gs.CreateTexture(v.attendancePopup,RM.ver.."_AttendanceTexture","BACKGROUND",300,600,"CENTER", v.attendancePopup, "CENTER", 0,0,[[Interface/ACHIEVEMENTFRAME/UI-GuildAchievement-Parchment]]);
				--Left Border--
				v.attendanceSideBorder=gs.CreateTexture(v.attendancePopup,RM.ver.."_AttendanceSideBorder","BORDER",13,650,"CENTER", v.attendancePopup, "CENTER", -150,-39,[[Interface/ACHIEVEMENTFRAME/UI-Achievement-MetalBorder-Left]]);
				--Bottom Border--
				v.attendanceTopBorder=gs.CreateTexture(v.attendancePopup,RM.ver.."_AttendanceTopBorder","BORDER",300,13,"CENTER", v.attendancePopup, "CENTER", 11,-300,[[Interface/ACHIEVEMENTFRAME/UI-Achievement-MetalBorder-Top]]);
				--BotRight Joint--
				v.attendanceBotRightJoint=gs.CreateTexture(v.attendancePopup,RM.ver.."_AttendanceBotRightJoint","BORDER",26,26,"CENTER", v.attendancePopup, "CENTER", 144,-293,[[Interface/ACHIEVEMENTFRAME/UI-Achievement-MetalBorder-Joint]]);
				--BotLeft Joint--
				v.attendanceBotLeftJoint=gs.CreateTexture(v.attendancePopup,RM.ver.."_AttendanceBotLeftJoint","BORDER",26,26,"CENTER", v.attendancePopup, "CENTER", -144,-293,[[Interface/ACHIEVEMENTFRAME/UI-Achievement-MetalBorder-Joint]]);
				RM:RotateTexture(v.attendanceBotLeftJoint, 270)
				
				--v.attendanceTopLeftJoint=gs.CreateTexture(v.attendancePopup,RM.ver.."_AttendanceTopLeftJoint","BORDER",10,668,"CENTER", v.attendancePopup, "CENTER", -100,-39,[[Interface/ACHIEVEMENTFRAME/UI-Achievement-MetalBorder-Joint]]);
				
			end
			v.attendancePopup:SetPoint('TOPRIGHT',UIParent,'TOPRIGHT',-50 ,-200)
			v.attendancePopup:SetToplevel(true);
			v.attendancePopup:SetWidth(300);
			v.attendancePopup:SetHeight(600);
			v.attendanceCloseButton:SetPoint("TOPRIGHT",v.attendancePopup,"TOPRIGHT",7,6);
			v.attendanceCloseButton:SetWidth(35);
			v.attendanceCloseButton:SetHeight(35);
			
			v.attendancePopup:Show()
			v.attendancePopup:SetScript("OnHide", function()
				if templayout then
					local cur_raid=RM:EmptySlots("remove", RM:deepCopy(templayout, {}))
				end
				if cur_raid then
					for pname, ptable in pairs(cur_raid) do
						if v[pname.."Checkbox"] then
							v[pname.."Checkbox"]:Hide()
						end
					end
					cur_raid=nil
				end
			end)
			RM:AttendancePopout()
		end)
	end
	options.show()
	v.difficultytext:SetText(difficulties[pDB.auto.difficulty]);
	v.difficultyframe:SetWidth(v.difficultytext:GetWidth());
	v.thresholdtext:SetText(RM.color[thresholds[pDB.auto.threshold].color]..thresholds[pDB.auto.threshold].qual.."|r")
	v.thresholdframe:SetWidth(v.thresholdtext:GetWidth());
	if pDB.auto.masterlooter then
		v.mastlootertext:SetText(RM.color["WHITE"]..pDB.auto.masterlooter.."|r,");
	else
		v.mastlootertext:SetText(RM.color["WHITE"]..UnitName("player").."|r,");
	end
	v.mastlootframe:SetWidth(v.mastlootertext:GetWidth());
	
	
	if pDB.auto.attendance==0 then
		v.autoattend:Disable()
		_G[v.autoattend:GetName() .. "Text"]:SetText(RM.color["RED"].."[REQUIRES DBM]|r Attendance Tracking on Boss Kill")
		v.autoattend:SetScript("OnEnter", function(self) RM:OnEnter(self,"autoattenddis") end)
		v.autoattend:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
	else
		v.autoattend:SetScript("OnEnter", function(self) RM:OnEnter(self,"autoattend") end)
		v.autoattend:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
		v.autoattend:SetChecked(pDB.auto.attendance)
		v.autoattend:SetScript("OnClick", function()
			if v.autoattend:GetChecked()==1 then
				pDB.auto.attendance=true
			else
				pDB.auto.attendance=false
			end
		end);
	end
	
	v.autopromote:SetChecked(pDB.auto.promote)
	v.autopromote:SetScript("OnClick", function()
		if v.autopromote:GetChecked()==1 then
			pDB.auto.promote=true
		else
			pDB.auto.promote=false
		end
	end);
	
	v.convertraidframe:SetChecked(pDB.auto.convertraid)
	v.convertraidframe:SetScript("OnClick", function()
		if v.convertraidframe:GetChecked()==1 then
			pDB.auto.convertraid=true
		else
			pDB.auto.convertraid=false
		end
	end);
	
	v.automastloot:SetChecked(pDB.auto.masterlooting)
	v.automastloot:SetScript("OnClick", function()
		if v.automastloot:GetChecked()==1 then
			pDB.auto.masterlooting=true
		else
			pDB.auto.masterlooting=false
		end
	end);
	
	v.disabletooltips:SetChecked(pDB.enable.tooltips)
	v.disabletooltips:SetScript("OnClick", function()
		if v.disabletooltips:GetChecked()==1 then
			pDB.enable.tooltips=true
		else
			pDB.enable.tooltips=false
		end
	end);
	
	v.disablelayouts:SetChecked(pDB.enable.transfers)
	v.disablelayouts:SetScript("OnClick", function()
		if v.disablelayouts:GetChecked()==1 then
			pDB.enable.transfers=true
		else
			pDB.enable.transfers=false
		end
	end);
end

function RM:CreateAttendance()
	
end

function RM:AttendancePopout()
	local x=10
	local y=-10
	if templayout then
		local cur_raid=RM:EmptySlots("remove", RM:deepCopy(templayout, {}))
		for pname, ptable in pairs(cur_raid) do
			if not v[pname.."Checkbox"] then
				v[pname.."Checkbox"]=CreateFrame("CheckButton",RM.ver.."_"..pname.."Checkbox",v.attendancePopup , "UICheckButtonTemplate");
			end
			v[pname.."Checkbox"]:SetPoint("TOPLEFT",v.attendancePopup, "TOPLEFT", x, y)
			_G[v[pname.."Checkbox"]:GetName() .. "Text"]:SetText(pname)
			_G[v[pname.."Checkbox"]:GetName() .. "Text"]:SetTextColor(RM.classcolor[templayout[pname].class].r, RM.classcolor[templayout[pname].class].g, RM.classcolor[templayout[pname].class].b)
			v[pname.."Checkbox"]:SetChecked(true)
			v[pname.."Checkbox"]:Show()
			y=y-23
			if y<=-580 then
				y=-10
				x=155
			end
		end
		cur_raid=nil
		return height
	end
end

function RM:AttendanceImport()
	EditBox:SetText("")
	v.editboxButton:SetScript("OnClick",function(self)
		if EditBox:GetText() == nil or EditBox:GetText() == "" then
			RM:Print("You did not import any text.")
		else
			RM:importAttendanceLUA(EditBox:GetText())
			AttendanceCopyFrame:Hide()
			EditBox:SetText("")
		end
	end)
	v.editboxButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"editboxbutton") end)
	v.editboxButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)
	
	EditBox:SetScript("OnEnterPressed",function(self)
		if EditBox:GetText() == nil or EditBox:GetText() == "" then
			RM:Print("You did not import any text.")
		else
			RM:importAttendanceLUA(EditBox:GetText())
			AttendanceCopyFrame:Hide()
			EditBox:SetText("")
		end
	end)
	AttendanceCopyFrame:SetClampedToScreen(true)
	AttendanceCopyFrame:SetFrameStrata("HIGH")
	AttendanceCopyFrame:Show()
end

function RM:AttendanceImportPopoutXML()
	v.editboxButton:SetScript("OnClick",function(self)
		if EditBox:GetText() == nil or EditBox:GetText() == "" then
			RM:Print("You did not import any text.")
		else
			RM:importAttendanceXML(EditBox:GetText())
			AttendanceCopyFrame:Hide()
			EditBox:SetText("")
		end
	end)
	v.editboxButton:SetScript("OnEnter", function(self) RM:OnEnter(self,"editboxbutton") end)
	v.editboxButton:SetScript("OnLeave", function(self) RM:OnLeave(self) end)

	EditBox:SetText("")
	EditBox:SetScript("OnEnterPressed",function(self)
		if EditBox:GetText() == nil or EditBox:GetText() == "" then
			RM:Print("You did not import any text.")
		else
			RM:importAttendanceXML(EditBox:GetText())
			AttendanceCopyFrame:Hide()
			EditBox:SetText("")
		end
	end)
	AttendanceCopyFrame:SetClampedToScreen(true)
	AttendanceCopyFrame:SetFrameStrata("HIGH")
	AttendanceCopyFrame:Show()
end

function RM:PopupWindow(output)
	if output then
		EditBox:SetText(output)
		EditBox:HighlightText()
	else
		EditBox:SetText("")
		EditBox:SetScript("OnEnterPressed",function(self)
			if EditBox:GetText() == nil or EditBox:GetText() == "" then
				RM:Print("You did not import any text.")
			else
				RM:importLayout(EditBox:GetText())
				AttendanceCopyFrame:Hide()
				EditBox:SetText("")
			end
		end)
	end
	AttendanceCopyFrame:SetClampedToScreen(true)
	AttendanceCopyFrame:SetFrameStrata("HIGH")
	AttendanceCopyFrame:Show()
end

function RM:OnEnter(self,arg)
	if pDB.enable.tooltips then
		GameTooltip:SetOwner(self, "ANCHOR_CURSOR")
		for i=1,#tooltips[arg] do
			GameTooltip:AddLine(tooltips[arg][i],1,1,1,1)
		end
		GameTooltip:Show()
	end
end

function RM:OnLeave(self)
	if pDB.enable.tooltips then
		GameTooltip:Hide()
		GameTooltip:ClearLines()
	end
end

function RM:RefreshOptions()
	RM:WipeList()
	v.title:SetText("Raid Mover")
	if v.maintabtexture:IsShown() then
		RM:CreateOptions()
	end
end

function RM:ToggleMover()
	if (RM.mainFrame:IsShown()==nil) then
		RM.mainFrame:Show();
	else 
		RM.mainFrame:Hide()
	end
end

function RM:RotateTexture(self, degrees)
	local angle = math.rad(degrees)
	local cos, sin = math.cos(angle), math.sin(angle)
	self:SetTexCoord((sin - cos), -(cos + sin), -cos, -sin, sin, -cos, 0, 0)
end

------  Minimap Button  ------
function RM:MinimapMenu()
	local savesList = {[1]="Cancel"}
	local i = 1
	for layoutname, layoutnames in pairs(pDB.layoutlist) do
		savesList[i] = 
			{ text = layoutnames, notCheckable=true, hasArrow = true,
				menuList = {
					{ text = "Restore", notCheckable=true, func = function() RM:startMover(layoutnames); end },
					{ text = "Invite", notCheckable=true, func = function() RM:inviteRaid(layoutnames); end },
					{ text = "Print", notCheckable=true, func = function() RM:PopupWindow(RM:printLayout(layoutnames)); end },
				},
			}
		i = i+1
	end
	
	if savesList == nil then
		RM:Print("You have no Saved Layouts")
	end
		
	local minimapmenu = {
		{ text = "Raid Mover", notCheckable=true, isTitle = true},
		{ text = "Saved Layouts", notCheckable=true, hasArrow = true,
			menuList = savesList
		},
		{ text = "Profiles", notCheckable=true, func = function() RM:showConfigWindow(); end },
	}
	local minimapmenuFrame = CreateFrame("Frame", "MiniMapMenuFrame", UIParent, "UIDropDownMenuTemplate")
	EasyMenu(minimapmenu, minimapmenuFrame, "cursor", 0 , 0, "MENU", 0.5);
end

function RM:ToggleMinimapButton()
	if RMIcon and not RMIcon:IsRegistered("Raid Mover") then
		RMIcon:Register("Raid Mover", RMLauncher, pDB.minimapicon)
	end
	if RMIcon then
		RMIcon:Refresh("Raid Mover", pDB.minimapicon)
		if pDB.minimapicon.hide==true then
			RMIcon:Show("Raid Mover")
			RM:Print("Showing Minimap Icon!")
			pDB.minimapicon.hide=false
		elseif pDB.minimapicon.hide==false then
			RMIcon:Hide("Raid Mover")
			RM:Print("Hiding Minimap Icon!")
			pDB.minimapicon.hide=true
		end
	end
end

function RM:CreateMinimapIcon()
	if RMLDB and RMIcon then
		RMIcon:Register("Raid Mover", RMLauncher, pDB.minimapicon)
	end
end
