local RM=LibStub("AceAddon-3.0"):GetAddon("Raid Mover");
--++++++++++++++++++++++++++   SLASH COMMANDS    ++++++++++++++++++++++++++++
function RM:initializeCommands()
    local commands = {}
    function commands.shorthelp()
        RM:Print(RM.color.CRIMSON.."===============USAGE===============|r")
        RM:Print(RM.color.STEELBLUE.."/raidmover|r or "..RM.color.STEELBLUE.."/rm [command]|r")
        RM:Print(RM.color.LEGENDARY.."invlayout, save, restore, remove, rename, copy, listsave, addplayer, removeplayer|r")
        RM:Print(RM.color.LEGENDARY.."Type|r  "..RM.color.STEELBLUE.."/rm help|r  for a description of each command")
        RM:Print(RM.color.LEGENDARY.."Type|r  "..RM.color.STEELBLUE.."/rm toggle|r  to show/hide Raid Mover")
		RM:Print(RM.color.LEGENDARY.."Type|r  "..RM.color.STEELBLUE.."/rm togglemap|r  to show/hide the minimap icon")
        RM:Print(RM.color.LEGENDARY.."Type|r  "..RM.color.STEELBLUE.."/rm profiles|r  to change the profile used")
    end

    function commands.more()
        RM:Print(RM.color.CRIMSON.."================HELP================|r")
        RM:Print(RM.color.STEELBLUE.."   /rm [command]|r args")
        RM:Print(RM.color.LEGENDARY.."invlayout:|r Invite players from a saved raid layout.")
		RM:Print(RM.color.STEELBLUE.."   /rm invlayout|r LAYOUT_NAME")
		RM:Print(RM.color.LEGENDARY.."save:|r Saves a raid layout")
		RM:Print(RM.color.STEELBLUE.."   /rm save|r LAYOUT_NAME")
		RM:Print(RM.color.LEGENDARY.."restore:|r Restores a raid layout")
		RM:Print(RM.color.STEELBLUE.."   /rm restore|r LAYOUT_NAME")
		RM:Print(RM.color.LEGENDARY.."remove:|r Removes a raid layout")
		RM:Print(RM.color.STEELBLUE.."   /rm remove|r LAYOUT_NAME")
		RM:Print(RM.color.LEGENDARY.."rename:|r Renames a raid layout")
		RM:Print(RM.color.STEELBLUE.."   /rm rename|r ORIGINAL_LAYOUT_NAME NEW_LAYOUT_NAME")
		RM:Print(RM.color.LEGENDARY.."copy:|r Copies a saved raid layout")
		RM:Print(RM.color.STEELBLUE.."   /rm copy|r ORIGINAL_LAYOUT_NAME NEW_LAYOUT_NAME")
		RM:Print(RM.color.LEGENDARY.."listsave:|r Lists your saved raid layouts, or add a layout name to list its contents")
		RM:Print(RM.color.STEELBLUE.."   /rm listsave|r LAYOUT_NAME")
		RM:Print(RM.color.LEGENDARY.."addplayer:|r Add a player to, or change a players group in a raid layout, case-sensitive, use 'DK' for Death Knights")
		RM:Print(RM.color.STEELBLUE.."   /rm addplayer|r LAYOUT_NAME PLAYER GROUP# CLASS")
		RM:Print(RM.color.LEGENDARY.."removeplayer:|r Removes a player from a raid layout, case-sensitive");
		RM:Print(RM.color.STEELBLUE.."   /rm removeplayer|r LAYOUT_NAME PLAYER");
    end
	
    function commands.help()
        commands.more()
    end
	
    function commands.toggle()
		RM:ToggleMover()
    end
	
	function commands.togglemap()
		RM:ToggleMinimapButton()
    end
	
    function commands.profiles()
        RM:showConfigWindow()
    end
		
	function commands.save(layout)
		if layout then
			RM:Print("Saving raid layout as: " ..layout)
			RM:saveRaid(layout)
		else
			RM:Print("ERROR: No layout name specified.")
		end
	end
	function commands.restore(layout)
		if layout then
			RM:startMover(layout)
		else
			RM:Print("ERROR: No layout name specified.")
		end
	end
	function commands.rename(origlayout, newlayout)
		if origlayout and newlayout then
			RM:renameLayout(origlayout, newlayout)
		else
			RM:Print("ERROR: That requires 2 layout names.")
		end
	end
	function commands.copy(origlayout, newlayout)
		if newlayout and origlayout then
			RM:copyLayout(origlayout, newlayout)
		else
			RM:Print("ERROR: That requires 2 layout names.")
		end
	end
	function commands.remove(layout)
		if layout then
			RM:removeLayout(layout);
		else
			RM:Print("ERROR: That requires a layout name.")
		end
	end
	function commands.create(layout)
		if layout then
			RM:createLayout(layout);
		else
			RM:Print("ERROR: No layout name specified.")
		end
	end
	
	function commands.listsave(layout)
		if layout then
			RM:PopupWindow(printLayout(layout))
		else
			RM:Print("Listing Saved Raid Layouts:")
			for k, v in pairs(pDB.layoutlist) do
				RM:Print(v)
			end
		end
	end

	function commands.addplayer(layoutname, player, subgrp, class)
		if layoutname and player and subgrp and class then
			RM:addPlayer(layoutname, player, subgrp, class)
		else 
			RM:Print("ERROR: Invalid arguments.")
		end
	end
	function commands.removeplayer(layoutname, playername)
		if layoutname and playername then
			RM:removePlayer(layoutname, playername);
		else
			RM:Print("ERROR: Invalid arguments.")
		end
	end

	function commands.runattendance(event, boss)
		RM:runAttendance(event, boss)
	end
	function commands.attendance()
		RM:runAttendanceTarget()
	end
	function commands.fixattendance()
		RM:Print("Fixing attendance records...")
		RM:fixAttendance()
	end
	function commands.attendancetarget()
		RM:runAttendanceTarget()
	end
	function commands.clearattendance(event, arg)
		RM:clearAttendance(event, arg)
	end
	
	--arg: "all_dates" "all_dates_boss" "all_dates_boss_diff" "all_kill" "all_kill_boss" "all_kill_boss_diff"
	function commands.killcount(arg, playername, boss, difficulty)
		RM:getKillCount(arg, playername, boss, difficulty)
	end

	function commands.getraidspecs()
		RM:getRaidSpecs()
	end
	
    self.commands = commands
	collectgarbage()
end

function RM:showConfigWindow()
    LibStub("AceConfigDialog-3.0"):Open("Raid Mover.Profiles")
end

function RM:onSlashCommand(str)
    local args = {self:GetArgs(str, 100)}
    local command = table.remove(args, 1)
	
    if not command then
        self.commands.shorthelp()
    elseif self.commands[command] then
        local fn = self.commands[command]
        fn(unpack(args))
    else
        RM:Print("No such command '" .. command .. "'")
    end
end
