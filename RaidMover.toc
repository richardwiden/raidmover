## Interface: 60000
## Title: Raid Mover
## Notes: Raid Mover is a lightweight addon that allows you to save and restore raid member group poisitions. 
## Author: Feja@Stormreaver-US
## Version: 1.40
## SavedVariables: RaidMoverDB
## X-Curse-Packaged-Version: 1.40
## X-Curse-Project-Name: Raid Mover
## X-Curse-Project-ID: raid-mover
## X-Curse-Repository-ID: wow/raid-mover/mainline
## X-Curse-Packaged-Version: r18
## X-Curse-Project-Name: Raid Mover
## X-Curse-Project-ID: raid-mover
## X-Curse-Repository-ID: wow/raid-mover/mainline

Raidmover.xml