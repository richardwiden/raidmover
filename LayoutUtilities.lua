local RM=LibStub("AceAddon-3.0"):GetAddon("Raid Mover");

function RM:saveLayout(name, layout)
	pDB.layouts[name] = {}
	pDB.layouts[name] = layout
end

function RM:removeLayout(layout)
	if pDB.layouts[layout] then
		pDB.layouts[layout] = nil;
		if not pDB.layouts[layout] then
			RM:Print(layout.." has been removed!");
		else
			RM:Print("ERROR: "..layout.." was NOT removed!");
		end
	else
		RM:Print("ERROR: "..layout.." does not exist!");
	end
	RM:runSaves();
end

function RM:createLayout(layout)
	if not pDB.layouts[layout] then
		pDB.layouts[layout] = {};
		if pDB.layouts[layout] then
			RM:Print(layout.." created successfully!")
		end
	else
		RM:Print("ERROR: "..layout.." already exists!");
	end
	RM:runSaves();
end

function RM:copyLayout(layoutname, copyname)
	if pDB.layouts[layoutname] and not pDB.layouts[copyname] then
		pDB.layouts[copyname] = RM:deepCopy(pDB.layouts[layoutname], {});
	elseif not pDB.layouts[layoutname] then
		RM:Print("ERROR: The layout "..layoutname.." does not exist.");
	else
		RM:Print("ERROR: A saved layout with the name "..copyname.." already exists.");
	end
	RM:runSaves();
end

function RM:renameLayout(layoutname, newname)
	if layoutname and newname and layoutname == newname then 
		RM:Print("ERROR: That is the same name!")
	elseif layoutname and newname and pDB.layouts[layoutname] and not pDB.layouts[copyname] then
		pDB.layouts[newname] = RM:deepCopy(pDB.layouts[layoutname], {});
		pDB.layouts[layoutname] = nil;
	elseif not pDB.layouts[layoutname] then
		RM:Print("ERROR: The layout "..layoutname.." does not exist.");
	elseif pDB.layouts[copyname] then
		RM:Print("ERROR: A saved layout with the name "..newname.." already exists.");
	else
		RM:Print("ERROR: Invalid arguments.");
	end	
	RM:runSaves();
end

function RM:printLayout(layout)
	local output = "";
	if layout then
		if pDB.layouts[layout] then
			for i=1, 8 do
				local header = 1;
				for pname, ptable in pairs(pDB.layouts[layout]) do
					if pDB.layouts[layout][pname].subgrp == i then
						if header == 1 then
							--DEBUG: RM:Print("--------- GROUP "..i.." ---------");
							output = output.."--------- GROUP "..i.." ---------\n";
							header = 0;
						end
						--DEBUG: RM:Print("          "..tostring(pname));
						output = output.."          "..tostring(pname).."\n";
					end
				end
			end
		else
			RM:Print(layout.." does not exist.");
		end
	else
		RM:Print("Invalid saved name.");
	end
	return output;
end

function RM:exportLayout(layoutname)
	local exportstring = "["..layoutname.."]\n"
	for i=1,8 do
		for pname, ptable in pairs(pDB.layouts[layoutname]) do
			if pDB.layouts[layoutname][pname].subgrp==i then
				exportstring = exportstring..pname..pDB.layouts[layoutname][pname].subgrp..pDB.layouts[layoutname][pname].class..",\n"
			end
		end
	end
	return exportstring
end

function RM:importLayout(importtext, layoutname)
	local layoutnamestring = "Added: "
	local testtext = importtext
	--Remove whitespace
	testtext = string.gsub(testtext, "%s+", "")
	testtext = string.gsub(testtext, "\n+", "")
	
	function find_layoutname(text)
		if #text>0 then
			local startindex = string.find(string.sub(text, 1, 2), "%[")
			if startindex then
				local endindex = string.find(text, "%]")
				--RM:Print(string.sub(text, startindex+1, endindex-1))
				pDB.layouts[string.sub(text, startindex+1, endindex-1)]={}
				layoutname = string.sub(text, startindex+1, endindex-1)
				layoutnamestring = layoutnamestring..layoutname..", "
				find_player(string.sub(text, endindex+1))
			else
				find_player(string.sub(text,1))
			end
		end
	end

	function find_player(text)
		local foundindex = string.find(text, ",")
		if foundindex then
			local numberindex = string.find(text, "%d+")
			--RM:Print(string.sub(text, 1, numberindex-1)..string.sub(text, numberindex, numberindex)..string.sub(text, numberindex+1, foundindex-1))
			pDB.layouts[layoutname][string.sub(text, 1, numberindex-1)]={}
			pDB.layouts[layoutname][string.sub(text, 1, numberindex-1)].subgrp=tonumber(string.sub(text, numberindex, numberindex))
			if string.sub(text, numberindex+1, foundindex-1)=="DeathKnight" or string.sub(text, numberindex+1, foundindex-1)=="Deathknight" then
				pDB.layouts[layoutname][string.sub(text, 1, numberindex-1)].class="Death Knight"
			else
				pDB.layouts[layoutname][string.sub(text, 1, numberindex-1)].class=string.sub(text, numberindex+1, foundindex-1)
			end
			find_layoutname(string.sub(text, foundindex+1))
		end
	end
	
	if layoutname then
		pDB.layouts[layoutname] = {};
		find_player(testtext)
		RM:Print(layoutnamestring..layoutname)
	else
		find_layoutname(testtext)
		RM:Print(layoutnamestring)
	end
	RM:runSaves();
	return(layoutname);
end