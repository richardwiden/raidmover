--[[
TODOS:
NVM 1 Checkbox page for Guild roster. 
	List ALL guild members(single column), even offline, in alphabetical order with input text field to the right side. Allow filtering by rank. Add a text field at the top with Save button to save selected players as a raid layout.

DONE! 2 Add /rm attendance (or I could just do /rm save Attendance and export from WTF folder? Won't have bosses killed tho) 
	-Needs to save every player currently in the raid to a proper excel format
	-When boss is killed catch DBMs kill announce, with date and time stamp, and save every member in raid(and zone?) while incrementing their boss kill count. Seperate Normal and Heroic.
	
DONE! 3 Add to invlist the options
	-Convert to Raid
	-set Heroic/Normal difficulty
	-set assistants, main tanks, loot master + threshold, raid leader.

DONE! 4 Add Minimap button with right click capability
	-Layouts popout option that displays all saved layouts, restores the selected layout.
	
5 Insert Attendance timestamp
	-Using one of the saved raid layouts
	Raid:Mover insertAttendance(layout, boss, difficulty, time, date)
	
DONE! 6 Refresh Layouts Dropdown in Config
	When saving a new layout do a update to the layout dropdown.
	
DONE! 7 Want to save player class as well for gui display purposes.
	Will have to change how I save a layout, currently it's 
	NEWLAYOUT = {PNAME1 = SUBGROUP#, PNAME2 = SUBGROUP#, PNAME3 = SUBGROUP#, etc... }  will have to change to
	NEWLAYOUT = {PNAME1 = {SUBGROUP#, CLASS}, PNAME2 = {SUBGROUP#, CLASS}, PNAME3 = {SUBGROUP#, CLASS}, etc...}
	thus changing how I restore a raid and print a raid
	OR
	just make a whole new table GETCLASS = {PNAME1 = CLASS, PNAME2 = CLASS}
	/script RepopMe()

8 Peon Jobs Done sound played when all raid moves complete.
PlaySoundFile("Sound\\Creature\\Peon\\PeonBuildingComplete1.wav")

9 Add an IsOnline check to attendence, maybe even an in same zone as me check

10 Add a popout tab to the right(bottom now) side of the screen for extra options
	-AutoAttendance
	-AutoPromotes
	
DONE! 11 Add a Class dropdown selection and cancel button when adding players

12 Force groups with empty slots to push players to the top.

DONE! 13 Buttons I NEEEED to add.
	-Delete Profile, Rename Profile, Cancel adding player
	
14 Real-Time option
	If selected then the movements in Raid Mover would actually happen real time in game, with a delay of course.
	Everytime someone is moved just send the switch to que runner, it should do auto waiting.
	
DONE! 15 Unsaved changes boolean
	When a player is Added, removed, switched places need to add an unsaved changes = true
	Then when a user presses Clear, Show Raid, or selects a new layout from Saves dropdown run the UnsavedChanges function for a new popup:
	"You have unsaved changes, are you sure you want to " .. reason .."?"
	Reasons would be "load a new layout", "clear this layout", or "show raid"
	
DONE! 16 Add Mass Player Additions to selected layouts. 
	Can use the dropdown option with multi-selection enabled. Then add pname+class to first empty slot, if not empty slots then ignore and continue with error message.

17 Add Attendance Tracking per boss popup selection frame
	Do a simple popup frame on the right side with a checkbox next to each players name, sorted by Group#
	"Create attendance record for the following players for "..bossname.."?"
	Add 2 buttons at the button labeled "Create" and "Cancel" , small small button for cancel.
	
18 Remove the Realm in players names for LFR.
   Add maybe a MeleeDPS or RangedDPS, Healer, Tank, icons too.
	
]]



RaidMover = LibStub("AceAddon-3.0"):NewAddon("Raid Mover", "AceTimer-3.0", "AceConsole-3.0", "AceComm-3.0", "AceSerializer-3.0", "AceEvent-3.0")
local RM=RaidMover;
RM.ver="RM1.30";
RM.view={};
RM.prefix="RaidMover";
RM.classcolor = {
	["NA"]={r=1, g=1, b=1, code="|CFF666666"},
	["Monk"]={r=0, g=1, b=0.59, code="|CFF00FF96"},
	["Druid"]={r=1.00, g=0.49, b=0.04, code="|CFFFF7D0A"},
	["Death Knight"]={r=0.77, g=0.12, b=0.23, code="|CFFC41F3B"},
	["Warrior"]={r=0.78, g=0.61, b=0.43, code="|CFFC79C6E"},
	["Warlock"]={r=0.58, g=0.51, b=0.79, code="|CFF9482C9"},
	["Mage"]={r=0.41, g=0.80, b=0.94, code="|CFF69CCF0"},
	["Priest"]={r=1.00, g=1.00, b=1.00, code="|CFFFFFFFF"},
	["Paladin"]={r=0.96, g=0.55, b=0.73, code="|CFFF58CBA"},
	["Shaman"]={r=0.0, g=0.44, b=0.87, code="|CFF2459FF"},
	["Rogue"]={r=1.00, g=0.96, b=0.41, code="|CFFFFF569"},
	["Hunter"]={r=0.67, g=0.83, b=0.45, code="|CFFABD473"}
	};
RM.color = {
	CYAN="|cff00ffff",
	LIGHTRED="|cffff6060",
	LIGHTBLUE="|cff00ccff",
	BLUE="|cff0000ff",
	GREEN="|cff00ff00",
	RED="|cffff0000",
	GOLD="|cffffcc00",
	ALICEBLUE="|cFFF0F8FF",
	ANTIQUEWHITE="|cFFFAEBD7",
	AQUA="|cff00ffff",
	ORANGE="|cffffa500",
	SPRINGGREEN="|cff00ff7f",
	STEELBLUE="|cff4682b4",
	YELLOW="|cFFFFFF00",
	PURPLE="|cFF800080",
	MAGENTA="|cFFFF00FF",
	CRIMSON="|cFFDC143C",
	ARTIFACT="|cffe6cc80",
	LEGENDARY="|cffff8000",
	EPIC="|cffa335ee",
	RARE="|cff0070dd",
	UNCOMMON="|cff1eff00",
	WHITE="|cffffffff",
	POOR="|cff9d9d9d",
};
RM.classes={
	["NA"]="NA",
	["Monk"]="Monk",
	["Druid"]="Druid",
	["DeathKnight"]="Death Knight",
	["DK"]="Death Knight",
	["Deathknight"]="Death Knight",
	["Warrior"]="Warrior",
	["Warlock"]="Warlock",
	["Mage"]="Mage",
	["Priest"]="Priest",
	["Paladin"]="Paladin",
	["Shaman"]="Shaman",
	["Rogue"]="Rogue",
	["Hunter"]="Hunter",
	};
------ Options GUI ------
local options = {
type = "group",
	args = {
		General = {
			order = 100,
			type = "group",
			name = "Raid Mover",
			desc = "General",
			args = {
				Intro = {
					order = 1,
					type = "description",
					name = "Raid Mover was designed with Priest healing in mind.\n\nIt's main focus is saving players' raid group positions to be restored prior to an encounter. Includes additional raid management features.\n\n",
				},
				ToggleMover = {
					order = 100,
					type = "execute",
					name = "Toggle Raid Mover",
					desc = "Toggles the main RaidMover window",
					func = function()
						RM:ToggleMover()
					end
				},
				MinimapBtn = {
					order = 120,
					type = "execute",
					name = "Toggle Minimap Button",
					desc = "Toggles showing or hiding of the minimap button",
					func = function() --function to run when pressed
						RM:ToggleMinimapButton()
					end,
				},
			},
		},
	},
};
local DEFAULTS={
	profile = {
		attendance={},
		auto= {
			["attendance"]=false, 
			["promote"]=false, 
			["promotelist"]={}, 
			["convertraid"]=false,
			["difficulty"]=3, 
			["masterlooting"]=false, 
			["masterlooter"]=false, 
			["threshold"]=4, 
		},
		enable= {["raidmover"]=true, ["tooltips"]=true, ["transfers"]=false},
		export= {["rank"]=false, ["class"]=false, ["date"]=false},
		fixattendancelist={},
		layouts={},
		layoutlist={},
		minimapicon = {hide = false, minimapPos = 220, radius = 80},
		movedelay=0.7,
		trackattendance=false,		
	},
};

------ GLOBALS ------
local groupdisplay = {};
local lockmemb = {};
local cur_raid = {};
local play_per_grp = {};
local que = {};
local layout = {};
local automove = false;
local setting_raid=false;
----------------------

function RM:OnInitialize()
	-----ACE DB INIT------
    self.db = LibStub("AceDB-3.0"):New("RaidMoverDB", DEFAULTS, true)
	self.db.RegisterCallback(self, "OnProfileChanged", "Update")
	self.db.RegisterCallback(self, "OnProfileCopied",  "Update")
	self.db.RegisterCallback(self, "OnProfileReset",   "Update")
	if not self.db then
		Print("Error: Database not loaded correctly. Please exit out of WoW and delete RaidMover.lua and RaidMover.lua.bak found in: \\World of Warcraft\\WTF\\Account\\<Account Name>>\\SavedVariables\\")
	end
	
	pDB = self.db.profile
	
	-----ACE DB OPTIONS INIT-----
	self.Profiles = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db)
	self.Profiles.name = "Profiles"

	LibStub("AceConfig-3.0"):RegisterOptionsTable("Raid Mover",          options)
	LibStub("AceConfig-3.0"):RegisterOptionsTable("Raid Mover.Profiles", self.Profiles)
	self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("Raid Mover", "Raid Mover")
	LibStub("AceConfigDialog-3.0"):AddToBlizOptions("Raid Mover.Profiles", self.Profiles.name, "Raid Mover")
	
	self:RegisterComm(RM.prefix);
	self:RegisterEvent("GROUP_ROSTER_UPDATE");
	self:RegisterChatCommand("raidmover", "onSlashCommand")
	self:RegisterChatCommand("rm", "onSlashCommand")
end

function RM:OnEnable()
	RM:Print("Loaded profile "..RM.color.CRIMSON.."\""..self.db:GetCurrentProfile().."\"|r");
	if pDB.auto.attendance==0 then
		pDB.auto.attendance=false
	end
	if DBM then
		DBM:RegisterCallback("kill", RM.runAttendance);
		--RM:Print("Found DBM, Auto Attendance Available!");
	else
		RM:Print("DBM not found, Auto Attendance "..RM.color.RED.."DISABLED|r.");
		pDB.auto.attendance=0
	end
		----RUN FUNCTIONS-----
	self:runSaves();
	RM:CreateMover()
	RM:CreateMinimapIcon();
	RM:initializeCommands()
end

function RM:runSaves()
	pDB.layoutlist = {}
	for layoutname, players in pairs(pDB.layouts) do
		if layoutname ~= "SavesDisplaySelected" then
			pDB.layoutlist[layoutname] = layoutname;
		end
	end
	pDB.layoutlist = RM:SortFunc(pDB.layoutlist)
end
function RM:SortFunc(myTable)
	local t = {}
	for title,value in RM:pairsByKeys(myTable) do
		table.insert(t, title)
	end
	myTable = t
	return myTable
end
function RM:pairsByKeys (t, f)
    local a = {}
    for n in pairs(t) do
    	table.insert(a, n)
    end
    table.sort(a, f)
    local i = 0      -- iterator variable
    local iter = function ()   -- iterator function
    	i = i + 1
    	if a[i] == nil then
    		return nil
    	else
    		return a[i], t[a[i]]
    	end
    end
    return iter
end
function RM:GROUP_ROSTER_UPDATE()
	if IsInRaid() and pDB.auto.promote and UnitIsGroupLeader("player") then --and #pDB.auto.promotelist>0 then
		for i = 1, MAX_RAID_MEMBERS do
			local name, rank = GetRaidRosterInfo(i)
			if name and (rank==0) then
				for p=1, #pDB.auto.promotelist do
					--RM:Print(pDB.auto.promotelist[p])
					--RM:Print("Name: "..name)
					if pDB.auto.promotelist[p]==name then
						RM:Print("Promoting "..name)
						PromoteToAssistant(name);
					end
				end
			end
		end
	end
end
function RM:Update(event, database, newProfileKey)
	pDB = database.profile
	RM:RefreshOptions()
end
function RM:Print(msg)
	DEFAULT_CHAT_FRAME:AddMessage(RM.color.CRIMSON.."[|r"..RM.color.YELLOW.."Raid Mover|r"..RM.color.CRIMSON.."]|r "..msg);
end


-- @param prefix A printable character (\032-\255) classification of the message (typically AddonName or AddonNameEvent)
-- @param text Data to send, nils (\000) not allowed. Any length.
-- @param distribution Addon channel, e.g. "RAID", "GUILD", etc; see SendAddonMessage API
-- @param target Destination for some distributions; see SendAddonMessage API
-- @param prio OPTIONAL: ChatThrottleLib priority, "BULK", "NORMAL" or "ALERT". Defaults to "NORMAL".
-- @param callbackFn OPTIONAL: callback function to be called as each chunk is sent. receives 3 args: the user supplied arg (see next), the number of bytes sent so far, and the number of bytes total to send.
-- @param callbackArg: OPTIONAL: first arg to the callback function. nil will be passed if not specified.

--function AceComm:SendCommMessage(prefix, text, distribution, target, prio, callbackFn, callbackArg)


--I can make Send anything I want
--What information do I want to send to other RaidMover addons?
---		1)Saved Layouts ofc.
---		2)Assist promotion list?
--
--Distribution? Can be "WHISPERS" "RAID" "GUILD"
--
function RM:Send(msg,layoutname,layout,chan,target)
	if msg=="sendlayout" then
		local dataTable={msg=msg, layoutname=layoutname, layout=layout};
		dataTable=self:Serialize(dataTable);
		RM:Print("Attempting to send "..layoutname.." to all RaidMovers in the raid...")
		self:SendCommMessage(RM.prefix,dataTable,chan,target);
	elseif msg=="layoutreceived" then
		local dataTable={msg=msg, layoutname=layoutname}
		dataTable=self:Serialize(dataTable)
		self:SendCommMessage(RM.prefix,dataTable,chan,target);
	end
end;

function RM:OnCommReceived(prefix, dataTable, chan, sender)
	local data=self:Deserialize(dataTable);
	if pDB.allowtransfer and prefix==RM.prefix and data.msg=="sendlayout" then
		--If name of layout exists append (i) to the end of it. 50 should be high enough.
		for i=1,50 do
			if not pDB.layouts[data.layoutname] then
				pDB.layouts[data.layoutname]={}
				pDB.layouts[data.layoutname]=data.layout
				RM:Print("Successfully received the layout "..data.layoutname.." from "..sender)
				--Send that I received it
				RM:Send("layoutreceived", data.layoutname, nil, "WHISPERS", sender)
				break
			elseif not pDB.layouts[data.layoutname.."("..i..")"] then
				pDB.layouts[data.layoutname.."("..i..")"]={}
				pDB.layouts[data.layoutname.."("..i..")"]=data.layout
				RM:Print("Successfully received the layout "..data.layoutname.."("..i..") from "..sender)
				--Send that I received it
				RM:Send("layoutreceived", data.layoutname.."("..i..")", nil, "WHISPERS", sender)
				break
			end
		end
		self:runSaves()
	elseif prefix==RM.prefix and data.msg=="layoutreceived" then
		RM:Print(sender.." successfully received "..data.layoutname)
	end
end


--+++++++++++++++++++++++   MAIN MOVER FUNCTIONS    ++++++++++++++++++++++++++++
function RM:startMover(layoutname)
	--Clearing Globals:
	lockmemb = {};
	cur_raid = {};
	play_per_grp ={};
	que = {};
	layout = {};
	
	if IsInRaid() and (pDB.layouts[layoutname] or type(layoutname) == "table") and not InCombatLockdown() and (UnitIsGroupAssistant("player") or UnitIsGroupLeader("player")) then
	
		cur_raid, play_per_grp  = self:getCurrentRaid();
		if type(layoutname) == "table" then
			layout = layoutname
		else
			layout = pDB.layouts[layoutname]
		end
		
		--SendChatMessage("<RaidMover> Initiating Raid Moves! Please keep your hands and feet inside the raid frames at all times...","RAID");
		
		for pname, ptable in pairs(layout) do
			if cur_raid[pname] and layout[pname].subgrp ~= cur_raid[pname].subgrp then
				self:mainSwap(pname, layout[pname].subgrp);
			elseif cur_raid[pname] and layout[pname].subgrp == cur_raid[pname].subgrp then
				lockmemb[pname] = true;
			else
				RM:Print(RM.classcolor[ptable.class].code..pname.."|r is not in the raid");
			end
		end
		self:queRunner();
	elseif not IsInRaid() then
		self:Print("ERROR: You are not in a raid.")	
	elseif not (UnitIsGroupAssistant("player") or UnitIsGroupLeader("player")) then
		self:Print("ERROR: You cannot move raid members.")	
	elseif InCombatLockdown() then
		self:Print("ERROR: You are in COMBAT! Cannot perform raid moves, try again after combat ends.")
	elseif not pDB.layouts[layoutname] then
		self:Print("ERROR: Layout "..tostring(layout).." does not exist.");
	else
		self:Print("ERROR: ALL HELL BROKE LOOSE!")
	end
end

function RM:mainSwap(pname, togroup)
	local counter = 0
	local temptable = {};
	--IF GROUP IS FULL:
	if play_per_grp[togroup] > 4 then
		for raider,v in pairs(cur_raid) do
			if cur_raid[raider].subgrp == togroup and raider ~= pname then
				if lockmemb and not lockmemb[raider] then
					--If the random raiders subgroup we're looking at matches the subgroup they need to be in from our saved layout, LOCK EM!
					if layout[raider] and cur_raid[raider].subgrp == layout[raider].subgrp and automove == false then
						lockmemb[raider] = true;
					--Else happens when the player is NOT locked and/or is NOT in our saved layout's assigned group
					else
						lockmemb[pname] = true;
						temptable["in_"] = pname;
						temptable["out"] = raider;
						table.insert(que, temptable);
						cur_raid[raider].subgrp = cur_raid[pname].subgrp;
						cur_raid[pname].subgrp = togroup;
						--Check to see if the person that got swapped out was randomly put in the group they need to be in.
						if layout[raider] and cur_raid[raider].subgrp==layout[raider].subgrp and automove==false then
							lockmemb[raider]=true;
						end
						return
					end
				end
				counter=counter+1
			end
		end
		
		if counter==5 then
			self:Print("Unable to move "..pname..". REASON: All players in group"..togroup.." are already locked.")
		end
		
	--ELSE GROUP IS EMPTY, MOVE PLAYER THERE:
	else
		play_per_grp[cur_raid[pname].subgrp]=play_per_grp[cur_raid[pname].subgrp]-1
		temptable["name"]=pname
		temptable["to"]=togroup;
		play_per_grp[togroup]=tonumber(play_per_grp[togroup])+1
		table.insert(que, temptable);
		cur_raid[pname].subgrp=togroup;
	end
end

function RM:queRunner()
	self:Print("Raid moves are in progress...");
	local count=0
	
	self.quetimer = self:ScheduleRepeatingTimer(
	function()
		if IsInRaid() and not InCombatLockdown() and (UnitIsGroupAssistant("player") or UnitIsGroupLeader("player")) then
			if #que > 0 then
				--If player was successfully moved, 'pop' them off the array.
				if que[1].in_ and RM:getRaidGrp(que[1].in_) == layout[que[1].in_].subgrp then
					table.remove(que,1)
					count=0
				elseif que[1].name and RM:getRaidGrp(que[1].name) == que[1].to then
					table.remove(que,1)
					count=0
				else
					if que[1].in_ and RM:getRaidGrp(que[1].in_) ~= layout[que[1].in_].subrp then
						if count==0 or count==2 then
							count=count+1
							SwapRaidSubgroup(RM:getRaidId(que[1].in_), RM:getRaidId(que[1].out));
						elseif count==1 or count==3 then
							count=count+1
						else
							count=0
							table.remove(que,1)
						end
					else
						if count==0 or count==2 then
							count=count+1
							SetRaidSubgroup(RM:getRaidId(que[1].name), que[1].to);
						elseif count==1 or count==3 then
							count=count+1
						else
							count=0
							table.remove(que,1)
						end
					end
				end
			else
				self:CancelTimer(self.quetimer)
				self:Print("End of moves, verifying the layout...");
				self:verifyLayout(layout)
			end
		elseif not IsInRaid() then
			self:Print("ERROR: You are not in a raid.")
			self:CancelTimer(self.quetimer)
		elseif not (UnitIsGroupAssistant("player") or UnitIsGroupLeader("player")) then
			self:Print("ERROR: You cannot move raid members anymore.")
			self:CancelTimer(self.quetimer)
		elseif InCombatLockdown() then
			self:Print("ERROR: You are in COMBAT! Cannot perform raid moves, try again after combat ends.")
			self:CancelTimer(self.quetimer)
		else
			self:Print("ERROR: SOMETHING BROKE!!!!")
			self:CancelTimer(self.quetimer)
		end
	end, pDB.movedelay)
end

function RM:verifyLayout(layout)
	local done = true;
	--self:Print("Verifying Layout...");
	local currentraid = self:getCurrentRaid();
	for pname, ptable in pairs(layout) do
		if currentraid[pname] and layout[pname].subgrp ~= currentraid[pname].subgrp then
			self:Print(pname .. " should be in group " .. layout[pname].subgrp);
			done = false;
		end
	end
	if done then
		self:Print(RM.color.GREEN.."All moves were successful!|r")
		-- SendChatMessage("<RaidMover> All raid moves were successful!","RAID")
	else
		self:Print("Raid layout unsuccessfully restored, try running it again.")
	end
end

function RM:realTimeMove(p_in, p_out, p_setgrp)
	lockmemb[pname] = true;
	temptable["in_"] = pname;
	temptable["out"] = raider;
	table.insert(que, temptable);
	cur_raid[raider].subgrp = cur_raid[pname].subgrp;
	cur_raid[pname].subgrp = togroup;
end

--+++++++++++++++++++++   RAID MANIPULATION UTILITIES    ++++++++++++++++++++++++++++

function RM:getPlayersPerGrp(layout)
	if type(layout)~="table" then
		layout=pDB.layouts[layout]
	end
	local playerspergrp = {[1]=0, [2]=0, [3]=0, [4]=0, [5]=0, [6]=0, [7]=0, [8]=0}
	for pname, ptable in pairs(layout) do
		playerspergrp[layout[pname].subgrp] = playerspergrp[layout[pname].subgrp]+1
	end
	
	return playerspergrp;
end

--Returns the raidID of pname as a number.
function RM:getRaidId(pname)
	if IsInRaid() then
		for i=1, MAX_RAID_MEMBERS do
			--gets name and subgrp of member with raidID i
			local name = GetRaidRosterInfo(i)
			if pname == name then
				return i;
			end
		end
	end
end

--Returns a players group # as format 'number'
function RM:getRaidGrp(pname)
	if IsInRaid() then
		for i=1, MAX_RAID_MEMBERS do
			--gets name and subgrp of member with raidID i
			local name,_,subgrp = GetRaidRosterInfo(i)
			if pname == name then
				return subgrp;
			end
		end
	end
end

--Returns a table of current raid layout and players per group in form:
--currentraid[playername].name / .subgroup / .id
function RM:getCurrentRaid()
	local raidlayout = {}
	local playersPerGroup = {0,0,0,0,0,0,0,0}
	for i = 1, MAX_RAID_MEMBERS do
		local name, rank, subgroup, level, class, fileName, zone, online, isDead, role, isML =
			GetRaidRosterInfo(i)
		
		if name then
			--Creates a table with player information name=Feja, id=raidID, and then subgroup
			raidlayout[name] = {}
			--raidlayout[name].name = name
			--raidlayout[name].id = i
			raidlayout[name].subgrp = subgroup
			--raidlayout[name].zone = zone
			raidlayout[name].class = class
			playersPerGroup[subgroup] = playersPerGroup[subgroup] + 1
			--DEBUG:
			--self:Print(raidlayout[name].name .. ", ID: " .. raidlayout[name].id .. " Group: " .. raidlayout[name].subgroup)
		end
	end
	return raidlayout, playersPerGroup
end

--DEBUG PURPOSES, lists all members of the raid and their current raid IDs
function RM:printRaid()
	local currentraid, playerpergrp  = self:getCurrentRaid();
	self:Print("Listing Current raid members and their ID's:")
	for pname,v in pairs(currentraid) do
		self:Print(pname..", ID: "..currentraid[pname].id);
	end
end

-- FROM: http://www.dreamincode.net/code/snippet5401.htm
-- USAGE:   newtable = deepCopy(tabletocopy, {});
function RM:deepCopy(a, b)
    if type(a) ~= "table" or type(b) ~= "table" then
        error("both parameters must be of type table but recieved " ..type(a).. 
          " and " .. type(b))
    else
        for k,v in pairs(a) do
            -- if the type is a table, we'll need to recurse.
            if type(v) ~= "table" then
                b[k] = v
            else
                local x = {}
                self:deepCopy(v, x)
                b[k] = x
            end        
        end
    end
    return b
end

function RM:deepPrint(e)
	local indent = "  ";
	-- if e is a table, we should iterate over its elements
	if type(e) == "table" then
		for k,v in pairs(e) do -- for every element in the table
			self:Print(indent..k)
			indent = indent..indent;
			self:deepPrint(v)       -- recursively repeat the same procedure
		end
	else -- if not, we can just print it
		self:Print(e)
	end
end

function RM:saveRaid(name)
	if name then
		if pDB.layouts[name] then
			RM:Print("ERROR: "..name.." already exists! Manually remove it first.")
		else
			pDB.layouts[name] = {}
			local raid = {}
				
			for i=1, MAX_RAID_MEMBERS do
				local pname, _, subgrp, _, class = GetRaidRosterInfo(i)
				if pname then
					pDB.layouts[name][pname]={}
					pDB.layouts[name][pname].subgrp=subgrp
					pDB.layouts[name][pname].class=class
				end
			end
			self:Print("Raid Layout \""..name.."\" Successfully Saved!")
			self:runSaves();
		end
	else
		RM:Print("ERROR: Invalid save name")
	end
end

function RM:inviteRaid(invitelist)
	if invitelist then
		RM:Print("Sending invites...")
		local count=0
		local mlootcount=0
		local raidcount=0
		local layout={}
		if type(invitelist) == "table" then
			layout=invitelist
		elseif pDB.layouts[invitelist] then
			layout=pDB.layouts[invitelist]
		else
			self:Print("ERROR: Cannot send invites, that layout does not exist.");
			return
		end
		
		--SET MASTERLOOT TIMER--
		if pDB.auto.masterlooting and GetLootMethod()~= "master" then
			self.masterloot_timer = self:ScheduleRepeatingTimer(
			function()
				mlootcount=mlootcount+1
				if (IsInRaid() or IsInGroup()) and UnitIsGroupLeader("player") then
					local method = GetLootMethod();
					if pDB.auto.masterlooting and method ~= "master" then
						SetLootMethod("master" ,pDB.auto.masterlooter, pDB.auto.threshold);
					end
					if GetLootThreshold() ~= pDB.auto.threshold then
						SetLootThreshold(pDB.auto.threshold);
					elseif method == "master" and GetLootThreshold() == pDB.auto.threshold then
						RM:Print("Master-Loot set to "..pDB.auto.masterlooter)
						self:CancelTimer(self.masterloot_timer)
					end
				end
				if mlootcount >=6 then
					RM:Print("ERROR: Unable to set Master-Loot after "..mlootcount.." tries, try doing it manually.")
					self:CancelTimer(self.masterloot_timer)
				end
			end, 2.5)
		end
		
		--CONVERT TO RAID AND DIFFICULTY TIMER--
		if pDB.auto.convertraid and pDB.auto.difficulty then
			self.raiddifficulty_timer = self:ScheduleRepeatingTimer(function()
				raidcount=raidcount+1
				if not IsInRaid() then
					ConvertToRaid()
				end
				if pDB.auto.difficulty and GetRaidDifficultyID()==pDB.auto.difficulty and IsInRaid() then
					self:CancelTimer(self.raiddifficulty_timer)
				elseif pDB.auto.difficulty and GetRaidDifficultyID()~=pDB.auto.difficulty then
					SetRaidDifficultyID(pDB.auto.difficulty)
				end
				if raidcount>=10 then
					RM:Print("ERROR: Unable to Convert to Raid after "..raidcount.." tries, try doing it manually.")
					self:CancelTimer(self.raiddifficulty_timer)
				end
			end, 1.5)
		end
		
		--MASS INVITE--
		local offline={}
		for i = 1, GetNumGuildMembers() do
			local gname, _, _, _, _, _, _, _, online = GetGuildRosterInfo(i)
			if not online and layout[gname] then
				offline[gname]=true
			end
		end
		for pname,subgrp in pairs(layout) do
			if count > 5 and not offline[pname] then
				if not IsInRaid() then
					if count==5 then
						self:ScheduleTimer(function()
							self:Print("Pausing invites for 5 seconds to allow group to raid conversion...")
						end, 1.25)
					end
					count=count+1
					self:ScheduleTimer(function()
						InviteUnit(pname);
					end, 5)
				else
					InviteUnit(pname);
				end
			elseif not offline[pname] then
				count=count+1
				InviteUnit(pname);
			end
		end
		local offlinestring="OFFLINE: "
		for pname, bool in pairs(offline) do
			offlinestring=offlinestring..RM.classcolor[layout[pname].class].code..pname.."|r, "
		end
		self:ScheduleTimer(function()
			RM:Print(offlinestring)
		end, 7)
	else
		self:Print("ERROR: Invalid invite layout name.");
	end
end

function RM:addPlayer(layoutname, pname, subgrp, class)
	subgrp = tonumber(subgrp)
	if not pDB.layouts[layoutname] then
		RM:Print("ERROR: "..layoutname.." does not exist! Create it first.")
	else
		local playpergrp = RM:getPlayersPerGrp(layoutname)
		if subgrp and subgrp < 9 and subgrp > 0 and classes[class] and pname and playerpergrp[subgrp]<5 then
			pDB.layouts[layoutname][pname] = {}
			pDB.layouts[layoutname][pname] = {subgrp=subgrp, class=classes[class]}
		elseif playerpergrp[subgrp]>4 then
			self:Print("ERROR: Group "..subgrp.." is already full!")
		else
			self:Print("ERROR: Invalid addplayer parameters.")
		end
	end
end

function RM:removePlayer(layoutname, pname)
	if pDB.layouts[layoutname] then
		if pDB.layouts[layoutname][pname] then
			pDB.layouts[layoutname][pname] = nil;
			if not pDB.layouts[layoutname][pname] then
				self:Print(pname.." successfully removed!");
			end
		else
			self:Print("ERROR: "..pname.." is not in the layout "..layoutname);
		end
	else
		RM:Print("ERROR: "..layoutname.." does not exist!")
	end
end



------UNFINISHED AUTO RAID MOVER CODE BELOW--------
function RM:startAutoMover()
	cur_raid, play_per_grp = {};
	local templayout = {};
	local temp_cur_raid = {};
	cur_raid, play_per_grp  = self:getCurrentRaid();
	--[[
	Create a 'virtual' layout then the system can be run just like my own layout.
	
	When I am creating this table, I need to find tanks first, then melee, then ranged, then healers.
	Adding them like temptable[counter#].role = pname ,role depending on the if statement.
	The question is do I do a repeat until, creating a temp cur_raid and removing people as I assign them a group? until temp_cur_raid == nil
	'For' loop inside a 'repeat until' loop?!
	]]
	self:getRaidSpecs();
	local temp_cur_raid = cur_raid;
	local counter = 1
	--TANKS--
	for pname, v in pairs(temp_cur_raid) do
		if temp_cur_raid[pname].spec == ("Blood" or "Guardian" or "Brewmaster" or "Protection") then
			templayout[pname] = math.ceil(counter/5);
			table.remove(temp_cur_raid, pname)
			counter = counter+1
		end
	end
	
	--MELEE--
	for pname, v in pairs(temp_cur_raid) do
		if temp_cur_raid[pname].spec == ("Frost" or "Unholy" or "Feral" or "Windwalker" or "Retribution" 
			or "Assassination" or "Combat" or "Subtlety" or "Enhancement" or "Arms" or "Fury") then
			templayout[pname] = math.ceil(counter/5);
			table.remove(temp_cur_raid, pname)
			counter = counter+1
		end
	end
	
	--RANGED--
	for pname, v in pairs(temp_cur_raid) do
		if temp_cur_raid[pname].spec == ("Balance" or "Beast Mastery" or "Marksmanship" or "Survival" or "Arcane" 
			or "Fire" or "Frost" or "Shadow" or "Elemental" or "Affliction" or "Demonology" or "Destruction") then
			templayout[pname] = math.ceil(counter/5);
			table.remove(temp_cur_raid, pname)
			counter = counter+1
		end
	end
	
	--HEALS--
	for pname, v in pairs(temp_cur_raid) do
		if temp_cur_raid[pname].spec == ("Restoration" or "Mistweaver" or "Holy" or "Discipline" or "Holy") then
			templayout[pname] = math.ceil(counter/5);
			table.remove(temp_cur_raid, pname)
			counter = counter+1
		end
	end
	--After these 4 for loops the entire raid should have been sorted as a layout by tank > melee > range > healer
	--Now just need to run the standard mainMove
	
	--pDB.layouts["FEJAS_DESIGNATED_AUTOMOVE_LAYOUT"] = templayout;
	pDB.layouts["Auto-Mover"] = templayout;
	self:runSaves();
	--self:mainMove("Auto-Move Layout")
end

function RM:getRaidSpecs()
	--local f = InS or CreateFrame("Frame","InS")
	local f = CreateFrame("Frame","InS")
	local specs={
	  -- from very helpful table at http://www.wowpedia.org/API_GetInspectSpecialization
		[250]="Blood", [251]="Frost", [252]="Unholy", [102]="Balance", [103]="Feral",
		[104]="Guardian", [105]="Restoration", [253]="Beast Mastery", [254]="Marksmanship",
		[255]="Survival", [62]="Arcane", [63]="Fire", [64]="Frost", [268]="Brewmaster",
		[270]="Mistweaver", [269]="Windwalker", [65]="Holy", [66]="Protection",
		[67]="Retribution", [256]="Discipline", [257]="Holy", [258]="Shadow",
		[259]="Assassination", [260]="Combat", [261]="Subtlety", [262]="Elemental",
		[263]="Enhancement", [264]="Restoration", [265]="Affliction", [266]="Demonology",
		[267]="Destruction", [71]="Arms", [72]="Fury", [73]="Protection",
		[0]="Unknown Spec"
	}
	
	-- returns a unit from a number 0=player 1-40=raid1-40 or party1-4 (well, 1-5)
	local function actualUnit(unitNumber)
		if unitNumber==0 then
			return "player"
		elseif IsInRaid() then
			return "raid"..unitNumber
		else
			RM:Print("Not in raid")
			return "party"..unitNumber
		end
	end
	
	--local function that assigns a players spec in the raid group
	f:SetScript("OnEvent", function(self,...)
		local unit = actualUnit(f.unit)
		local cur_raid = {}
		RM:Print(UnitName(unit),"is a",specs[GetInspectSpecialization(unit)],(UnitClass(unit)))
		--cur_raid[UnitName(unit)].spec = specs[GetInspectSpecialization(unit)].." "..(UnitClass(unit));
		repeat
			-- some units may be out of inspect range, which would never trigger an
			-- INSPECT_READY.  Increment unit counter until we find someone in range
			f.unit = f.unit+1
			unit = actualUnit(f.unit)
			if CheckInteractDistance(unit,1) then
				NotifyInspect(unit) -- and inspect them
				return
			end
		until f.unit>GetNumGroupMembers()
		self:UnregisterEvent("INSPECT_READY")
		return cur_raid
	end)
	
	f:RegisterEvent("INSPECT_READY")
	f.unit = 0 -- arbitrary unit counter, starting at 0 for "player"
	NotifyInspect(actualUnit(f.unit))
	--return cur_raid
end

local TALENTS_NA = NOT_APPLICABLE:lower();
local TALENTS_NONE = NO.." "..TALENTS;
-- Option Constants
local CACHE_SIZE = 25;		-- Change cache size here (Default 25)
local INSPECT_DELAY = 0.2;	-- The time delay for the scheduled inspection
local INSPECT_FREQ = 2;		-- How often after an inspection are we allowed to inspect again?

-- Variables
local current = {};

-- Time of the last inspect reuqest. Init this to zero, just to make sure. This is a global so other addons could use this variable as well
lastInspectRequest = 0;
function RM:gatherTalents(isInspect)
	local spec = isInspect and GetInspectSpecialization(current.unit) or GetSpecialization();
	if (not spec or spec == 0) then
		current.format = TALENTS_NONE;
	elseif (isInspect) then
		local _, specName = GetSpecializationInfoByID(spec);
		--local _, specName = GetSpecializationInfoForClassID(spec,current.classID);
		current.format = specName or TALENTS_NA;
	else
		-- MoP Note: Is it no longer possible to query the different talent spec groups anymore?
--		local group = GetActiveSpecGroup(isInspect) or 1;	-- Az: replaced with GetActiveSpecGroup(), but that does not support inspect?
		local _, specName = GetSpecializationInfo(spec);
		current.format = specName or TALENTS_NA;
	end
end

function IsInspectFrameOpen() 
	return (InspectFrame and InspectFrame:IsShown()) or (Examiner and Examiner:IsShown())
end