local RM=LibStub("AceAddon-3.0"):GetAddon("Raid Mover");

function RM.runAttendance(event, mod) --/script RaidMover.runAttendance("kill",{combatInfo={name="Ultraxion"}})
	if pDB.auto.attendance then
		local boss = mod.combatInfo.name;
		local difficulties={[3]="10N",[4]="25N",[5]="10H",[6]="25H"};
		local logdate = date("%m/%d/%y %H:%M");
		local attendance = {};
		
		RM:Print(boss.. " killed! ");
		
		local difficulty = difficulties[GetRaidDifficultyID()];
		local logcount = 0;
		for i=1, MAX_RAID_MEMBERS do
			--local name, rank, subgroup, level, class, fileName, zone, online, isDead, role, isML = GetRaidRosterInfo(i)
			local pname, _, _, _, _, _, zone = GetRaidRosterInfo(i)
			if pname then
				if not pDB.attendance[pname] or not pDB.attendance[pname][boss] or not pDB.attendance[pname][boss][difficulty] then
					if not pDB.attendance[pname] or not pDB.attendance[pname][boss] then
						if not pDB.attendance[pname] then
							pDB.attendance[pname] = {};
						end
						pDB.attendance[pname][boss] = {};
					end
					pDB.attendance[pname][boss][difficulty] = {logdate};
					logcount = logcount + 1
				else
					table.insert(pDB.attendance[pname][boss][difficulty], logdate);
					logcount = logcount + 1
					if not pDB.attendance[pname][boss][difficulty].kills then
						pDB.attendance[pname][boss][difficulty].kills=1
					else
						pDB.attendance[pname][boss][difficulty].kills= pDB.attendance[pname][boss][difficulty].kills+1
					end
				end
			end
		end
		RM:Print(logcount.." attendance records saved for "..boss.."'s kill on "..logdate);
	end
end

function RM:runAttendanceTarget()
	--Debug to clear: pDB.attendance = {};
	local difficulties={[3]="10N",[4]="25N",[5]="10H",[6]="25H"};
	local logdate = date("%m/%d/%y %H:%M");
	local attendance = {};
	local boss = ""
	if UnitName("target") then
		boss=UnitName("target")
	else
		boss="Attended"
	end

	local difficulty = difficulties[GetRaidDifficultyID()];
	local logcount = 0;
	for i=1, MAX_RAID_MEMBERS do
		--local name, rank, subgroup, level, class, fileName, zone, online, isDead, role, isML = GetRaidRosterInfo(i)
		local pname, _, _, _, _, _, zone = GetRaidRosterInfo(i)
		if pname then
			if not pDB.attendance[pname] or not pDB.attendance[pname][boss] or not pDB.attendance[pname][boss][difficulty] then
				if not pDB.attendance[pname] or not pDB.attendance[pname][boss] then
					if not pDB.attendance[pname] then
						pDB.attendance[pname] = {};
					end
					pDB.attendance[pname][boss] = {};
				end
				pDB.attendance[pname][boss][difficulty] = {logdate};
				logcount = logcount + 1
			else
				table.insert(pDB.attendance[pname][boss][difficulty], logdate);
				logcount = logcount + 1
			end
		end
	end
	RM:Print(logcount.." attendance records saved for "..boss.."'s kill on "..logdate);
end

function RM:clearAttendance(event, arg)
	if event == "all" then
		pDB.attendance = {};
		self:Print("All attendance records deleted!")
	elseif event == "player" then
		pDB.attendance[arg] = nil;
		self:Print("All attendance records for "..arg.." deleted!")
	elseif event == "boss" then
		for pname, v in pairs(pDB.attendance) do
			if pDB.attendance[pname][arg] then
				pDB.attendance[pname][arg] = nil;
				self:Print(arg.." Attendance Record for "..pname.." deleted!")
			end
		end
		self:Print("All attendance records for "..arg.." deleted!")
	elseif event == "difficulty" then
		self:Print("Not implemented");
	end
end

function RM:importAttendanceXML(importtext)
	local layoutnamestring = "Added: "
	local maintext = string.gsub(importtext, "\n+", "")
	--RM:Print(maintext)
	local findtext
	local endindex
	local pname
	local boss
	local difficulty
	local kills
	
	function find_pname(text)
		if #text>10 then
			----Find Player----
			local _, pnamestart=string.find(string.sub(text, 1, 50), "<Name>")
			endindex=string.find(string.sub(text, 1, 80), "</Name>")
			if pnamestart and endindex then
				RM:Print("Pnamestart: "..pnamestart)
				RM:Print("Endindex: "..endindex)
				pname=string.sub(text, pnamestart+1, endindex-1)
				RM:Print(pname)
				find_boss(string.sub(text, endindex))
			else
				return true
			end	
		end
	end
	
	function find_boss(text)
		local _, bossstart=string.find(string.sub(text, 1, 20), "<Boss>")
		endindex=string.find(string.sub(text, 1, 60), "</Boss>")
		if bossstart and endindex then
			RM:Print("Bossstart: "..bossstart)
			RM:Print("Endindex: "..endindex)
			boss=string.sub(text, bossstart+1, endindex-1)
			RM:Print(boss)
			find_diff(string.sub(text,endindex))
		else
			RM:Print("ERROR: Invalid XML input, could not find a boss for"..pname)
			return false
		end
	end
	
	function find_diff(text)
		local _, diffstart=string.find(string.sub(text, 1, 30), "<Difficulty>")
		endindex=string.find(string.sub(text, 1, 60), "</Difficulty>")
		if diffstart and endindex then
			RM:Print("Diffstart: "..diffstart)
			RM:Print("Endindex: "..endindex)
			difficulty=string.sub(text, diffstart+1, endindex-1)
			RM:Print(difficulty)
			find_kills(string.sub(text,endindex))
		else
			RM:Print("ERROR: Invalid XML input, could not find a difficulty for "..pname.." on boss "..boss)
			return false
		end
	end
	
	function find_kills(text)
		local _, killstart=string.find(string.sub(text, 1, 30), "<Kills>")
		endindex=string.find(string.sub(text, 1, 55), "</Kills>")
		if killstart and endindex then
			RM:Print("Killstart: "..killstart)
			RM:Print("Endindex: "..endindex)
			kills=string.sub(text, killstart+1, endindex-1)
			RM:Print(kills)
			
			if not pDB.attendance then
				pDB.attendance={}
			end
			if not pDB.attendance[pname] then
				pDB.attendance[pname]={}
			end
			if not pDB.attendance[pname][boss] then
				pDB.attendance[pname][boss]={}
			end
			if not pDB.attendance[pname][boss][difficulty] then
				pDB.attendance[pname][boss][difficulty]={}
			end
			pDB.attendance[pname][boss][difficulty].kills=kills
			
			find_pname(string.sub(text,endindex))
		else
			RM:Print("ERROR: Invalid XML input, could not find killcount for "..pname.." on "..difficulty.." "..boss)
			return false
		end
	end

	if maintext then
		find_pname(maintext)
	end
	--return(layoutname);
end

function RM:exportAttendanceXML()
	--local guild = GetNumGuildMembers(true);
	--output = "\<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<guild>\n";
	output = "\<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<attendance>\n";
	--for i = 1, guild do
	--	name, rank, rankIndex, level, class, zone, note, officernote, online, status, classFileName, achievementPoints, achievementRank, isMobile = GetGuildRosterInfo(i);
	--	if pDB.attendance[name] then
		for name, namestable in pairs(pDB.attendance) do
			for boss, difficulties in pairs(pDB.attendance[name]) do
				for difficulty, dtable in pairs(pDB.attendance[name][boss]) do
					output = output .. "<player>\n<Name>".. name .. "</Name>\n"
					-- if pDB.export.rank then
						-- output = output .. "<Rank>" .. rank .. "</Rank>\n"
					-- end
					-- if pDB.export.class then
						-- output = output .. "<Class>" .. class .. "</Class>\n"
					-- end
					output = output .. "<Boss>" .. boss .. "</Boss>\n"
					output = output .. "<Difficulty>" .. difficulty .. "</Difficulty>\n"
					if pDB.attendance[name][boss][difficulty].kills then
						output = output .. "<Kills>" .. pDB.attendance[name][boss][difficulty].kills .. "</Kills>\n"
					else
						output = output .. "<Kills>" .. #pDB.attendance[name][boss][difficulty] .. "</Kills>\n"
					end
					if pDB.export.date then
						for i=1, #pDB.attendance[name][boss][difficulty] do
							 output = output .. "<Date" .. i .. ">" .. pDB.attendance[name][boss][difficulty][i] .. "</Date" .. i .. ">\n"
						end
					end
					output = output .. "</player>\n"
				end
			end
		end
	--end
	output = output .. "</attendance>";
	
	return output;
end

function RM:exportGuildAttendanceXML()
	local guild = GetNumGuildMembers(true);
	--output = "\<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<guild>\n";
	output = "\<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<attendance>\n";
	for i = 1, guild do
		name, rank, rankIndex, level, class, zone, note, officernote, online, status, classFileName, achievementPoints, achievementRank, isMobile = GetGuildRosterInfo(i);
		if pDB.attendance[name] then
			for boss, difficulties in pairs(pDB.attendance[name]) do
				for difficulty, dtable in pairs(pDB.attendance[name][boss]) do
					output = output .. "<player>\n<Name>".. name .. "</Name>\n"
					if pDB.export.rank then
						output = output .. "<Rank>" .. rank .. "</Rank>\n"
					end
					if pDB.export.class then
						output = output .. "<Class>" .. class .. "</Class>\n"
					end
					output = output .. "<Boss>" .. boss .. "</Boss>\n"
					output = output .. "<Difficulty>" .. difficulty .. "</Difficulty>\n"
					if pDB.attendance[name][boss][difficulty].kills then
						output = output .. "<Kills>" .. pDB.attendance[name][boss][difficulty].kills .. "</Kills>\n"
					else
						output = output .. "<Kills>" .. #pDB.attendance[name][boss][difficulty] .. "</Kills>\n"
					end
					if pDB.export.date then
						for i=1, #pDB.attendance[name][boss][difficulty] do
							 output = output .. "<Date" .. i .. ">" .. pDB.attendance[name][boss][difficulty][i] .. "</Date" .. i .. ">\n"
						end
					end
					output = output .. "</player>\n"
				end
			end
		end
	end
	output = output .. "</attendance>";
	
	return output;
end

function RM:exportAttendanceLUA()
	local guild = GetNumGuildMembers(true);
	outstring = "{\n";
	
	for pname, bosstable in pairs(pDB.attendance) do
		if pname ~= "enabled" then
			outstring = outstring.."\t[\""..pname.."\"]".."={\n"
			for boss, difficultytable in pairs(pDB.attendance[pname]) do
				outstring = outstring.."\t\t[\""..boss.."\"]".."={\n"
				for difficulty, kills in pairs(pDB.attendance[pname][boss]) do
					outstring = outstring.."\t\t\t[\""..difficulty.."\"]".."={\n"
					if pDB.attendance[pname][boss][difficulty][1] then
						for	i=1, #pDB.attendance[pname][boss][difficulty] do
							if pDB.attendance[pname][boss][difficulty][i] then
								outstring = outstring.."\t\t\t\t["..i.."]=\""..pDB.attendance[pname][boss][difficulty][i].."\",\n"
							end
						end
					elseif pDB.attendance[pname][boss][difficulty].kills then
						outstring = outstring.."\t\t\t\t[\"kills\"]="..pDB.attendance[pname][boss][difficulty].kills..",\n"
					end
					outstring = outstring.."\t\t\t},\n"
				end
				outstring = outstring.."\t\t},\n"
			end
			outstring = outstring.."\t},\n"
		end
	end
	outstring = outstring.."}"
	return outstring
end

--USED TO FIX/CLEANUP OLD ATTENDANCE RECORDS
function RM:fixAttendance(addboss)
	if addboss then
		table.insert(pDB.fixattendancelist, addboss)
		return
	end
	local guild = GetNumGuildMembers(true);
	for pname, bosstable in pairs(pDB.attendance) do
		if pname ~= "enabled" then
			--RM:Print(bosstable)
			for boss, difficultytable in pairs(pDB.attendance[pname]) do
				--RM:Print(difficultytable)
				if boss == "Gekkan" or boss == "Instructor Chillheart" or boss == "Xin the Weaponmaster" or boss == "Trial of the King" or boss == "Jandice Barov" or boss == "Arena of Annihilation" or boss == "Arena Of Annihilation" or boss == "Thalnos the Soulrender" or boss == "Brother Korloff" or boss == "High Inquisitor Whitemane" or boss == "Zandalari Blade Initiate" or boss=="A Brewing Storm" or boss=="Gu Cloudstrike" then
					RM:Print("Removed "..boss.." for "..pname)
					pDB.attendance[pname][boss] = nil
				end
			end
			if next(pDB.attendance[pname])==nil then
				RM:Print("Clearing "..pname)
				pDB.attendance[pname]=nil
			end
		end
	end
end

--/rm killcount(all_kills)
--arg: "dates" "all_dates_boss" "all_dates_boss_diff" "kills" "all_kill_boss" "all_kill_boss_diff"
function RM:getKillCount(arg, pname, boss, difficulty)
	local indent = "  ";
	local outstring=""
	--local allowprint = true;
	
	--[[local function DeepPrint(e)
		-- if e is a table, we should iterate over its elements
		if type(e) == "table" then
			for k,v in pairs(e) do -- for every element in the table
				self:Print(indent..k)
				--indent = indent..indent;
				DeepPrint(v)       -- recursively repeat the same procedure
				--if killcounter > 0 then
				--	self:Print(indent..killcounter)
				--	killcounter = 0;
				--end
			end
		else -- if not, we can just print it
			if allowprint then
				self:Print(e)
			else
				killcounter = killcounter + 1
			end
		end
	end
	
	if arg == "all_dates" then
		--TODO: Code to print [and return] the dates of every kill for each difficulty and boss
		DeepPrint(pDB.attendance[pname]);
	elseif arg == "all_dates_boss" then
		--TODO: Code to print and return the dates of every kill on 'boss' for each difficulty
		DeepPrint(pDB.attendance[pname][boss]);
	elseif arg == "all_dates_boss_diff" then
		--TODO: Code to print and return the dates of every kill on 'boss' for 'difficulty'
		DeepPrint(pDB.attendance[pname][boss][difficulty]);
	elseif arg == "all_kill" then
		--TODO: Code to print and return the number of kills for each difficulty and boss
		allowprint = false
		DeepPrint(pDB.attendance[pname]);
	elseif arg == "all_kill_boss" and bosses == boss then
		--TODO: Code to print and return the number of kills on 'boss' for each difficulty
		allowprint = false
		DeepPrint(pDB.attendance[pname][boss]);
	elseif arg == "all_kill_boss_diff" then
		--TODO: Code to print and return the number of kills on 'boss' for 'difficulty'
		allowprint = false
		DeepPrint(pDB.attendance[pname][boss][difficulty]);
	end
	]]

	for bosses, difficulties in pairs(pDB.attendance[pname]) do
		--self:Print(bosses)
		if arg == "dates" or arg == "kills" then
			outstring = outstring..bosses
			if pDB.attendance[pname][bosses]["25N"] then
				outstring = outstring..", 25N: "..#pDB.attendance[pname][bosses]["25N"]
			end
			if pDB.attendance[pname][bosses]["25H"] then
				outstring = outstring..", 25H: "..#pDB.attendance[pname][bosses]["25H"]
			end
			if pDB.attendance[pname][bosses]["10N"] then
				outstring = outstring..", 10N: "..#pDB.attendance[pname][bosses]["10N"]
			end
			if pDB.attendance[pname][bosses]["10H"] then
				outstring = outstring..", 10H: "..#pDB.attendance[pname][bosses]["10H"]
			end
			self:Print(outstring)
			outstring=""
		elseif boss and boss == bosses then
			self:Print(boss)
		end
		if arg == "dates" then
			for kills, dates in pairs(difficulties) do --'kills' is the killcounts
				self:Print("DEBUG: kills "..kills);
				--self:Print("DEBUG: kills "..kills);
				--self:Print("DEBUG: dates "..dates);
				if arg == "all_dates" then
					self:Print("  "..kills..") "..dates)
				elseif arg == "all_dates_boss" and bosses == boss then
					--TODO: Code to print and return the dates of every kill on 'boss' for each difficulty
					self:Print("  "..kills..") "..dates)
				elseif arg == "all_dates_boss_diff" and difficulties == difficulty then
					self:Print("  "..kills..") "..dates)
				end
			end
		end
	end
end